<?php
/**
 * TCU Web Standard functions and definitions.
 *
 * @package tcu_web_standards
 * @since 1.0.0
 */

/**
 * INCLUDE NEEDED FILES
 */

// Core includes.
require_once 'library/tcu-core.php';

// Customizes the UI.
require_once 'library/admin/tcu-customize.php';

/**
 * OEMBED SIZE OPTIONS
 */

if ( ! isset( $content_width ) ) {
	$content_width = 735;
}

/**
 * THUMBNAIL SIZE OPTIONS
 */

add_image_size( 'tcu-thumb-600', 600, 150, true );
add_image_size( 'tcu-thumb-300', 300, 100, true );

add_filter( 'image_size_names_choose', 'tcu_custom_image_sizes' );

// Let's make this pluggable for our child themes.
if ( ! function_exists( 'tcu_custom_image_sizes' ) ) {

	/**
	 * Call back function to 'image_size_names_choose'
	 * The following will add a new image size option to the list of selectable sizes in the Media Library.
	 *
	 * @param array $sizes  Array of image sizes.
	 */
	function tcu_custom_image_sizes( $sizes ) {
		return array_merge(
			$sizes, array(
				'tcu-thumb-600' => __( '600px by 150px', 'tcu_web_standards' ),
				'tcu-thumb-300' => __( '300px by 100px', 'tcu_web_standards' ),
			)
		);
	}
}

/**
 * ACTIVE SIDEBARS & WIDGETIZES AREAS
 */

// Let's make this pluggable for our child themes.
if ( ! function_exists( 'tcu_register_sidebars' ) ) {

	/**
	 * Register our main sidebar
	 */
	function tcu_register_sidebars() {
		register_sidebar(
			array(
				'id'            => 'sidebar1',
				'name'          => __( 'Main Sidebar', 'tcu_web_standards' ),
				'description'   => __( 'The primary sidebar.', 'tcu_web_standards' ),
				'before_widget' => '<div id="%1$s" class="tcu-sidebar__widget %2$s cf">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="tcu-sidebar__widget-title">',
				'after_title'   => '</h4>',
			)
		);
	}
}

// Let's make this pluggable for our child themes.
if ( ! function_exists( 'tcu_register_footer_widgets' ) ) {

	/**
	 * Register our footer widgets.
	 */
	function tcu_register_footer_widgets() {

		register_sidebar(
			array(
				'id'            => 'tcu_wordmark',
				'name'          => __( 'Footer Wordmark', 'tcu_web_standards' ),
				'description'   => __( 'The wordmark widget area located in the footer. Please add an image size of 300px by 300px.', 'tcu_web_standards' ),
				'before_widget' => '<div id="%1$s" class="tcu-footer__widget %2$s cf">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 id="tcu-wordmark" class="tcu-footer__widget-title h4">',
				'after_title'   => '</h5>',
			)
		);

		register_sidebar(
			array(
				'id'            => 'tcu_contact_info-1',
				'name'          => __( 'Footer Contact 1', 'tcu_web_standards' ),
				'description'   => __( 'The first widget area to add contact information located in the footer', 'tcu_web_standards' ),
				'before_widget' => '<div id="%1$s" class="tcu-footer__widget %2$s cf">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 class="tcu-footer__widget-title h4">',
				'after_title'   => '</h5>',
			)
		);

		register_sidebar(
			array(
				'id'            => 'tcu_contact_info-2',
				'name'          => __( 'Footer Contact 2', 'tcu_web_standards' ),
				'description'   => __( 'The second widget area to add contact information located in the footer', 'tcu_web_standards' ),
				'before_widget' => '<div id="%1$s" class="tcu-footer__widget %2$s cf">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 class="tcu-footer__widget-title h4">',
				'after_title'   => '</h5>',
			)
		);
	}
}

/**
 * COMMENT LAYOUT
 */

// Let's make this pluggable for our child themes.
if ( ! function_exists( 'tcu_comments' ) ) {

	/**
	 * Callback function to display comments
	 *
	 * Displays all comments for a post or Page based on a variety of parameters including ones set in the
	 * administration area.
	 *
	 * @param array $comment Array obtained by get_comments query.
	 * @param array $args    The options for the function.
	 * @param array $depth   How deep (in comment replies) should the comments be fetched.
	 */
	function tcu_comments( $comment, $args, $depth ) {
		?>
		<li <?php comment_class(); ?>>
		<div id="comment-<?php comment_ID(); ?>" class="cf">
		<header class="tcu-comments__author tcu-vcard cf">
			<?php

			/*
				This is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular WordPress gravatar call:
				echo get_avatar($comment,$size='32',$default='<path_to_url>' );
			*/

			// custom gravatar call.
			// create variable.
			$bgauthemail = get_comment_author_email();
			?>
			<img data-gravatar="http://www.gravatar.com/avatar/<?php echo esc_html( md5( $bgauthemail ) ); ?>?s=64" class="load-gravatar avatar avatar-48 photo" height="64" width="64" src="http://0.gravatar.com/avatar/34a46a0e179833a3859aee5e6256c158?s=64&d=mm&r=g" />
			<?php // end custom gravatar call. ?>

			<?php
				/* translators: %s: Author link */
				printf( __( '<cite class="fn">%s</cite>', 'tcu_web_standards' ), get_comment_author_link() );
			?>
				<time datetime="<?php echo comment_time( 'Y-m-j' ); ?>"><a href="<?php echo esc_url( htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ); ?>"><?php comment_time( __( 'F jS, Y', 'tcu_web_standards' ) ); ?> </a></time>
				<?php edit_comment_link( esc_html__( '(Edit)', 'tcu_web_standards' ), '  ', '' ); ?>
				</header>
				<?php if ( '0' === $comment->comment_approved ) : ?>
				<div class="tcu-alert tcu-alert--info tcu-left-100">
					<p><?php esc_html_e( 'Your comment is awaiting moderation.', 'tcu_web_standards' ); ?></p>
				</div>
				<?php endif; ?>
				<section aria-label="Response from a user that submitted a comment" class="tcu-comments__content cf">
					<?php comment_text(); ?>
				</section>
				<?php
				comment_reply_link(
					array_merge(
						$args, array(
							'depth'     => $depth,
							'max_depth' => $args['max_depth'],
						)
					)
				);
				?>
				</div>
		<?php // </li> is added by WordPress automatically ?>
	<?php
	}
}

/**
 * SEARCH FORM LAYOUT
 */
// Let's make this pluggable for our child themes.
if ( ! function_exists( 'tcu_wpsearch' ) ) {

	/**
	 * Display search form.
	 *
	 * @param string $form  The HTML for the search form.
	 */
	function tcu_wpsearch( $form ) {
		$form = '<form role="search" method="get" class="tcu-searchform cf" action="' . home_url( '/' ) . '" >
				<label class="screen-reader-text tcu-visuallyhidden tcu-searchform__label" for="s">' . __( 'Search for:', 'tcu_web_standards' ) . '</label>
                <input class="tcu-searchform__text" type="search" value="' . get_search_query() . '" name="s" id="s" placeholder="' . esc_attr__( 'Search the Site...', 'tcu_web_standards' ) . '" />
                <input title="Search" class="tcu-searchform__submit" type="submit" value="Go">
				</form>';

		return $form;
	}
}

/**
 * YOUTUBE VIDEO
 */
add_filter( 'embed_oembed_html', 'tcu_custom_oembed_filter', 10, 4 );

// Let's make this pluggable to our child themes.
if ( ! function_exists( 'tcu_custom_oembed_filter' ) ) {

	/**
	 *  YOUTUBE VIDEO
	 * We are wrapping all our videos in a video-container wrapper
	 * This ensures that our videos are responsive
	 *
	 * @param string $html    The HTML wrapper of the video.
	 * @return string $return The wrapped video
	 */
	function tcu_custom_oembed_filter( $html ) {
		$return = '<div class="video-container">' . $html . '</div>';
		return $return;
	}
}

/**
 * SHORTCODES
 */

// ADDS ABILITY TO USE SHORTCODE IN WIDGET AREA.
add_filter( 'widget_text', 'do_shortcode' );

/**
 * Adds a shortcode for our primary button
 *
 * @param array  $atts      List of attributes.
 * @param string $content   The shortcode content.
 *
 * @return string $html     The HTML for the shortcode.
 */
function tcu_button( $atts, $content = null ) {

	$args = shortcode_atts(
		array(
			'link'       => '#',
			'aria-label' => false,
			'id'		 => false,
		), $atts
	);

	$html = '<a ';

	// Only display if aria-label is available.
	if ( $args['aria-label'] ) {
		$html .= 'aria-label="' . $args['aria-label'] . '"';
	}

	// Only display if ID is available.
	if ( $args['id'] ) {
		$html .= 'id="' . $args['id'] . '"';
	}

	$html .= ' class="tcu-button tcu-button--primary" href="' . $args['link'] . '">' . do_shortcode( $content ) . '</a>';

	return $html;
}

add_shortcode( 'button', 'tcu_button' );

/**
 * Adds a shortcode for our secondary button
 *
 * @param array  $atts      List of attributes.
 * @param string $content   The shortcode content.
 *
 * @return string $html     The HTML for the shortcode.
 */
function tcu_button_secondary( $atts, $content = null ) {
	$args = shortcode_atts(
		array(
			'link'       => '#',
			'aria-label' => false,
			'id'		 => false,
		), $atts
	);

	$html = '<a';

	// Only display if aria-label is available.
	if ( $args['aria-label'] ) {
		$html .= ' aria-label="' . $args['aria-label'] . '"';
	}

	// Only display if ID is available.
	if ( $args['id'] ) {
		$html .= ' id="' . $args['id'] . '"';
	}

	$html .= ' class="tcu-button tcu-button--secondary" href="' . $args['link'] . '">' . do_shortcode( $content ) . '</a>';

	return $html;
}

add_shortcode( 'button_secondary', 'tcu_button_secondary' );

/**
 * Adds a shortcode for a quote block
 *
 * @param array  $atts      List of attributes.
 * @param string $content   The shortcode content.
 *
 * @return string $html     The HTML for the shortcode.
 */
function tcu_pull_quote( $atts, $content = null ) {
	return '<div class="tcu-quote">
				<span class="tcu-quote__top">
					<svg height="57" width="57">
						<use xlink:href="#top-quote"></use>
					</svg>
				</span>'
					. do_shortcode( $content ) .
				'<span class="tcu-quote__btm">
					<svg height="57" width="57">
						<use xlink:href="#btm-quote"></use>
					</svg>
				</span>
			</div>';
}

add_shortcode( 'quote', 'tcu_pull_quote' );

/**
 * Adds a shortcode for TWO COLUMNS
 *
 * @param array  $atts      List of attributes.
 * @param string $content   The shortcode content.
 *
 * @return string $html     The HTML for the shortcode.
 */
function tcu_two_columns( $atts, $content = null ) {
	return '<div class="tcu-two-columns tcu-below16 tcu-top16 cf">' . do_shortcode( $content ) . '</div>';
}

add_shortcode( 'two_columns', 'tcu_two_columns' );

/**
 * Adds a shortcode for THREE COLUMNS
 *
 * @param array  $atts      List of attributes.
 * @param string $content   The shortcode content.
 *
 * @return string $html     The HTML for the shortcode.
 */
function tcu_three_columns( $atts, $content = null ) {
	return '<div class="tcu-three-columns tcu-below16 tcu-top16 cf">' . do_shortcode( $content ) . '</div>';
}

add_shortcode( 'three_columns', 'tcu_three_columns' );

if ( ! function_exists( 'tcu_fix_shortcodes' ) ) {

	/**
	 * Remove paragraph issue
	 *
	 * @param string $content   HTML content.
	 * @return string $content  Filtered content.
	 */
	function tcu_fix_shortcodes( $content ) {

		$array = array(
			'<p>['    => '[',
			']</p>'   => ']',
			']<br />' => ']',
		);

		$content = strtr( $content, $array );
		return $content;
	}
	add_filter( 'the_content', 'tcu_fix_shortcodes' );
}

/**
 * Register our  TinyMCE button.
 *
 * @param array $buttons Array of button options.
 */
function tcu_mce_buttons( $buttons ) {
	array_push( $buttons, '|', 'tcu_mce_button' );
	return $buttons;
}

/**
 * Points to the path of the JS file for the TinyMCE button
 *
 * @param array $plugin_array  The Registered TinyMCE button.
 * @return array $plugin_array The registered TinyMCE button with JS path.
 */
function tcu_add_plugin( $plugin_array ) {
	$plugin_array['tcu_mce_button'] = get_template_directory_uri() . '/library/js/min/tcu-button.min.js';
	return $plugin_array;
}

/**
 * Add TinyMCE button to WYSIWYG
 */
function tcu_shortcode_button() {
	// check user permissions.
	if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
		return;
	}
	// check if WYSIWYG is enabled.
	if ( get_user_option( 'rich_editing' ) === 'true' ) {
		add_filter( 'mce_external_plugins', 'tcu_add_plugin' );
		add_filter( 'mce_buttons', 'tcu_mce_buttons' );
	}
}

add_action( 'init', 'tcu_shortcode_button' );

/**
 * Tiny MCE enable hidden buttons
 *
 * @param array $buttons List of buttons.
 *
 * @return array $buttons
 */
function tcu_enable_more_buttons( $buttons ) {
	$buttons[] = 'fontsizeselect';
	$buttons[] = 'backcolor';

	return $buttons;
}

add_filter( 'mce_buttons_3', 'tcu_enable_more_buttons' );

// Customize mce editor font sizes.
if ( ! function_exists( 'tcu_mce_text_sizes' ) ) {
	/**
	 * Add font more font sizes
	 *
	 * @param array $init_array The font size button.
	 * @return array $init_array The font size button with string of font options.
	 */
	function tcu_mce_text_sizes( $init_array ) {
		$init_array['fontsize_formats'] = '8px 10px 12px 14px 16px 18px 20px 22px 24px 26px 28px 30px 36px 40px 50px 60px 70px 80px';
		return $init_array;
	}
}

add_filter( 'tiny_mce_before_init', 'tcu_mce_text_sizes' );

/**
 *  Add Styleselect with approved fonts
 *
 * @param array $buttons Second-row list of buttons.
 * @return array $buttons
 */
function tcu_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}

add_filter( 'mce_buttons_2', 'tcu_mce_buttons_2' );

/**
 * Callback function to filter the MCE settings
 *
 * @param array $init_array This filter grants developers access to the TinyMCE settings array.
 *
 * @return array $init_array
 */
function tcu_mce_before_init_insert_formats( $init_array ) {

	// Define the style_formats array.
	$style_formats = array(
		// Each array child is a format with it's own settings.
		array(
			'title'   => 'Cabin',
			'inline'  => 'span',
			'classes' => 'tcu-cabin',
			'wrapper' => true,

		),
		array(
			'title'   => 'Eurostile',
			'inline'  => 'span',
			'classes' => 'tcu-eurostile',
			'wrapper' => true,
		),
		array(
			'title'   => 'Moonface',
			'inline'  => 'span',
			'classes' => 'tcu-moonface',
			'wrapper' => true,
		),
		array(
			'title'   => 'Arvo',
			'inline'  => 'span',
			'classes' => 'tcu-arvo',
			'wrapper' => true,
		),
		array(
			'title'   => 'Grey Background',
			'block'   => 'div',
			'classes' => 'tcu-entry-content__bg-grey',
			'wrapper' => true,
		),
	);

	// Insert the array, JSON ENCODED, into 'style_formats'.
	$init_array['style_formats'] = wp_json_encode( $style_formats );

	return $init_array;
}

// Attach callback to 'tiny_mce_before_init'.
add_filter( 'tiny_mce_before_init', 'tcu_mce_before_init_insert_formats' );

?>
