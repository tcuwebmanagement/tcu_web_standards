<?php
/**
 * The template that displays 404 errors.
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 1.0.0
 */

// Let's make sure nobody can access this page directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php get_header(); ?>

	<div class="tcu-layoutwrap--transparent">

		<div class="tcu-layout-constrain cf">

			<main class="unit m-size2of3 size2of3 cf">

				<?php
				/**
				 * We add #main name anchor to our content element because we have a skip
				 * main navigation link for accessibility
				 */
				?>
				<a name="main" tabindex="-1" id="main"><span class="tcu-visuallyhidden"><?php esc_html_e( 'Main Content', 'tcu_web_standards' ); ?></span></a>

				<?php get_template_part( 'partials/content', 'error' ); ?>

			</main><!-- end of .unit -->

			<?php get_sidebar(); ?>

			</div><!-- end of .tcu-layout-constrain -->

	</div><!-- end of .tcu-layoutwrap--transparent -->

<?php get_footer(); ?>
