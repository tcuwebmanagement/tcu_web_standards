<?php
/**
 * The template for displaying the header.
 *
 * Displays all of the head elements.
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 1.0.0
 */

?><!doctype html>
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available. ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title( '|', true, 'left' ); ?></title>

		<?php // Mobile meta. ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<?php // Icons & favicons. ?>
		<link rel="apple-touch-icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.png">

		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.ico">
		<![endif]-->

		<?php // favicon.ico for IE10 win. ?>
		<meta name="msapplication-TileColor" content="#4d1979">
		<meta name="msapplication-TileImage" content="<?php echo esc_url( get_template_directory_uri() ); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

		<?php // WordPress head functions. ?>
		<?php wp_head(); ?>
		<?php // end of WordPress head. ?>
	</head>

	<body <?php body_class(); ?>>

		<div class="tcu-layout-container">

			<header class="tcu-header cf" role="banner">

				<?php // Our SVG Library - Do not remove! ?>
				<?php require_once 'library/images/svg/sprite.symbol.svg'; ?>

				<?php
				/**
				 * Make sure to have a container with the #main ID
				 * include it in the main content.
				 */
				?>
				<a href="#main" class="tcu-skip-nav tcu-visuallyhidden"><?php esc_html_e( 'Skip to main content', 'tcu_web_standards' ); ?></a>

				<div class="tcu-layoutwrap--dark tcu-layoutwrap--dark--border tcu-not-mobile">

					<?php // .nav-universal__is-mobile for meanmenu.js. ?>
					<ul class="tcu-layout-constrain tcu-nav-universal tcu-nav-universal__is-mobile cf">
						<li class="tcu-nav-universal__icons cf">
							<a href="http://www.tcu.edu/a_to_z.asp"><svg focusable="false" width="15" height="25"><use xlink:href="#list-icon"></use></svg>
								<?php esc_html_e( 'A-Z directory', 'tcu_web_standards' ); ?>
							</a>
						</li>
						<li class="tcu-nav-universal__icons cf">
							<a href="http://www.makeagift.tcu.edu"><svg focusable="false" width="15" height="25"><use xlink:href="#gift-icon"></use></svg>
								<?php esc_html_e( 'Make a gift', 'tcu_web_standards' ); ?>
							</a>
						</li>
						<li class="tcu-nav-universal__icons tcu-has-children menu-item-has-children cf">
							<a tabindex="-1" href="#"><svg focusable="false" width="15" height="25"><use xlink:href="#person-icon"></use></svg>
								<?php esc_html_e( 'I\'m looking for...', 'tcu_web_standards' ); ?>
							</a>
							<ul class="tcu-dropdown tcu-dropdown--grey tcu-unstyled-list sub-menu cf">
								<li><a href="http://admissions.tcu.edu/"><?php esc_html_e( 'Apply', 'tcu_web_standards' ); ?></a></li>
								<li><a href="http://www.admissions.tcu.edu/Visit-TCU"><?php esc_html_e( 'Visit', 'tcu_web_standards' ); ?></a></li>
								<li><a href="https://my.tcu.edu"><?php esc_html_e( 'MyTCU', 'tcu_web_standards' ); ?></a></li>
								<li><a href="https://mail.tcu.edu"><?php esc_html_e( 'My TCU Email', 'tcu_web_standards' ); ?></a></li>
							</ul>
						</li>
						<li class="tcu-search-form__mobile"><?php get_search_form(); ?></li>
					</ul>
				</div><!-- end of .tcu-layoutwrap--dark -->

				<div class="tcu-layout-constrain cf">
					<a class="tcu-banner__logo" href="http://www.tcu.edu/" rel="nofollow">
					<img height="113" width="129" src="<?php bloginfo( 'template_url' ); ?>/library/images/svg/tcu-logo-banner.svg" alt="Texas Christian University logo">
					<span class="tcu-visuallyhidden">
					<?php esc_html_e( 'Texas Christian University', 'tcu_web_standards' ); ?></span>
				</a>
					<div class="tcu-header__site-title"><a aria-label="<?php bloginfo( 'name' ); ?> - Homepage" href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo( 'name' ); ?></a></div>
				</div>

				<?php // #menu-main-menu for meanmenu.js ?>
				<nav aria-label="Primary" class="tcu-main-nav tcu-layoutwrap--purple tcu-pad-t0 tcu-pad-b0">
					<?php
					wp_nav_menu(
						array(
							'container'      => false,
							'menu'           => __( 'The Main Menu', 'tcu_web_standards' ),
							'menu_class'     => 'tcu-layout-constrain tcu-top-nav cf',
							'theme_location' => 'main-nav',
							'depth'          => 2,
						)
					);
					?>
				</nav>
			</header><!-- end of .tcu-header -->
