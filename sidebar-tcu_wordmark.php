<?php
/**
 * The template for displaying the tcu_wordmark widget in footer.
 * No longer used because the 'id' of the registered sidebar has changed.
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 1.0.0
 *
 * @deprecated 4.7.0 No longer used by internal code and not recommended.
 * @see filename sidebar-tcu-wordmark.php
 */

?>
<div class="tcu-footer-departments__wordmark size1of3 m-size1of2 unit">

	<?php if ( is_active_sidebar( 'tcu_wordmark' ) ) : ?>

		<?php dynamic_sidebar( 'tcu_wordmark' ); ?>

	<?php else : ?>

		<?php // This content shows up if there are no widgets defined in the backend. ?>

		<div class="tcu-alert tcu-alert--help">
			<p><?php __( 'Please add the wordmark.', 'tcu_web_standards' ); ?></p>
		</div>

	<?php endif; ?>

</div><!-- end of .tcu-footer-departments__wordmark -->
