# TCU Web Standards

**Web Standards Theme for TCU**

The TCU Web Standards theme is designed to incorporate TCU's Brand Standards. This theme contains TCU's design system modules.

**GETTING STARTED**

We use GULP as our task runner. All our WordPress production files are located in `dist` directory. When you are ready to ship the next theme version, update the version number in `package.json` and run `npm run build` or `gulp build` to compress a clean copy of the next theme version. The `build` script will run `gulp build` to compile and copy files into the dist directory and then it will run `gulp zip` to compress a clean copy of the next theme version.

To start developing:

`gulp watch` Automatically handle changes to CSS, JS, SVGs, and PHP files. Also kicks off BrowserSync.

**Our Files**

We offer two versions — a minified version, and an un-minified one. Use the minified version in a production environment or to reduce the file size of your downloaded assets. And the un-minified version is better if you are in a development environment or would like to debug the CSS or JavaScript assets in the browser.

**Our main SASS files are**

    library/scss

**WordPress Theme**

Run `gulp zip` to compress a clean zip version of the `dist` directory. From the WordPress admin screen, select Appearance > Themes from the menu. Click on Add new and select the zip file you just created to install. Finally, activate the theme.

    dist/

**jQuery**

WordPress comes with jQuery installed. Make sure to include jQuery if you are not using a WordPress theme.

**Working with an existing Gulp project**

Assuming that the `Gulp CLI` has been installed and that the project has already been configured with a package.json and a Gulpfile, it's very easy to start working with Gulp:

* Change to the project's root directory.
* Install project dependencies with `npm install`.
* Run Gulp with `gulp`.

**Gulp Tasks**

`gulp watch` Automatically handle changes to CSS, JS, SVGs, and PHP files. Also kicks off BrowserSync.

`gulp icons` Creates an SVG sprite. Only use this to update/edit current icon library.

`gulp scripts` Compile JS files through babel and then uglify.

`gulp sprites` Create a sprite of our PNGs. Currently, we do not use this yet.

`gulp lint` Run Javascript against WordPress code standards and run Sass against WordPress code standards.

`gulp copy` Copy a clean (without Git files, Gulp, package.json and so on) WordPress theme version to the `dist` directory.

`gulp build` Compiles css and js, optimizes images, and then copies the WordPress theme version to the `dist` directory.

`gulp update-version` Updates dist/style.css with the package.json version number.

`gulp zip` Creates a zip file with the contents of the `dist` directory.

`gulp` Runs the following tasks at the same time: scripts, styles, imagemin.

**Install NPM**

**[Download NPM](https://www.npmjs.com/)**

Developed by **Mayra Perales**: <mailto:m.j.perales@tcu.edu>

## Special thanks to:

    - Eddie Machado - http://themble.com/bones
    - Paul Irish & the HTML5 Boilerplate
    - Yoast for some WP functions & optimization ideas
    - Andrew Rogers for code optimization
    - David Dellanave for speed & code optimization
    - and several other developers. :)

## Submit Bugs & or Fixes:

_[Create an issue](https://bitbucket.org/TCUWebmanage/tcu_web_standards/issues?status=new&status=open)_

## Meta

* [Changelog](CHANGELOG.md)
