<?php
/**
 * The template to display the sidebar1
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 1.0.0
 */

?>

<aside class="tcu-sidebar unit size1of3 m-size1of3 cf">

	<?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>

		<?php dynamic_sidebar( 'sidebar1' ); ?>

	<?php else : ?>

		<?php // This content shows up if there are no widgets defined in the backend. ?>

		<div class="tcu-alert tcu-alert--help">
			<p><?php __( 'Please activate some Widgets.', 'tcu_web_standards' ); ?></p>
		</div>

	<?php endif; ?>

</aside><!-- end of .tcu-sidebar -->
