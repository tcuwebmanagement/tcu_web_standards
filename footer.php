<?php
/**
 * The footer file for our theme.
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 1.0.0
 */

?>
		<footer class="tcu-footer cf">
			<div class="tcu-footer-departments">
				<div class="tcu-layout-constrain tcu-footer-departments__widgets below24 cf">
					<?php
						get_sidebar( 'tcu_wordmark' );
						get_sidebar( 'tcu_contact_info1' );
						get_sidebar( 'tcu_contact_info2' );
					?>
				</div>
			</div>

			<div class="tcu-footer-main cf">

				<?php // The background image of the TCU Fountain. ?>
				<div class="tcu-footer__fountain"></div>

				<div class="tcu-footer__inner cf">

					<a class="tcu-logo tcu-logo--white" href="http://www.tcu.edu/"><img height="70" width="143" src="<?php bloginfo( 'template_url' ); ?>/library/images/svg/tcu-logo.svg" alt="Texas Christian University logo"><span class="tcu-visuallyhidden"><?php esc_html_e( 'Texas Christian University', 'tcu_web_standards' ); ?></span></a>

					<p class="tcu-layout-constrain tcu-footer__inner__links">
						<a href="http://www.maps.tcu.edu/" title="Maps &amp; Directions"><?php esc_html_e( 'Maps &amp; Directions', 'tcu_web_standards' ); ?></a>
					</p>

					<div class="tcu-layout-constrain tcu-footer__inner__text" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
						<span itemprop="streetAddress"><?php esc_html_e( '2800 South University Drive', 'tcu_web_standards' ); ?><br>
							<?php esc_html_e( 'Fort Worth, Texas 76109', 'tcu_web_standards' ); ?> <br></span>
						<span itemprop="telephone"><?php esc_html_e( '817-257-7000', 'tcu_web_standards' ); ?></span>
					</div>

					<nav aria-label="Secondary" class="tcu-layoutwrap--dark">
						<ul class="tcu-footer__inner__wrap__nav tcu-layout-constrain cf">
							<li><a href="https://hr.tcu.edu/work-at-tcu"><?php esc_html_e( 'Work at TCU', 'tcu_web_standards' ); ?></a></li>
							<li><span> / </span><a href="https://accessibility.tcu.edu/"><?php esc_html_e( 'Accessibility', 'tcu_web_standards' ); ?></a></li>
							<li><span> / </span><a href="https://www.tcu.edu/compliance/notice-of-nondiscrimination.php"><?php esc_html_e( 'Notice of Nondiscrimination', 'tcu_web_standards' ); ?></a></li>
							<li><span> / </span><a href="https://www.tcu.edu/institutional-equity/title-ix/index.php"><?php esc_html_e( 'Title IX', 'tcu_web_standards' ); ?></a></li>
							<li><span> / </span><a href="https://www.tcu.edu/compliance/legal-disclosures.php"><?php esc_html_e( 'Legal Disclosures', 'tcu_web_standards' ); ?></a></li>
							<li><span> / </span><a href="https://www.tcu.edu/compliance/privacy.php"><?php esc_html_e( 'Privacy', 'tcu_web_standards' ); ?></a></li>
							<li><span> / </span><a href="https://ie.tcu.edu/about/accreditation/"><?php esc_html_e( 'Accreditation', 'tcu_web_standards' ); ?></a></li>
						</ul>
					</nav>

				</div><!-- end of .tcu-footer__inner -->

				<div class="tcu-footer__inner__copyright cf">

					<div class="group unit size1of1 m-size1of1">
						<div class="tcu-socialmedia unit m-size1of1 size1of2">
							<?php
							/**
							 * We are giving our social icons a class name
							 * so we add an PNG for IE 8 support
							 */
							?>
							<a class="tcu-facebook" href="https://www.facebook.com/pages/Fort-Worth-TX/TCU-Texas-Christian-University/155151195064" title="The official TCU's Facebook account"><svg focusable="false" height="30" width="30"><use xlink:href="#facebook"></use></svg><span class="tcu-visuallyhidden"><?php esc_html_e( 'Facebook', 'tcu_web_standards' ); ?></span></a>

							<a class="tcu-flickr" href="https://www.flickr.com/photos/texaschristianuniversity/" title="The official TCU's  Flickr account"><svg focusable="false" height="30" width="30"><use xlink:href="#flickr"></use></svg><span class="tcu-visuallyhidden"><?php esc_html_e( 'Flickr', 'tcu_web_standards' ); ?></span></a>

							<a class="tcu-instagram" href="https://www.instagram.com/texaschristianuniversity/" title="The official TCU's  Instagram account"><svg focusable="false" height="30" width="30"><use xlink:href="#instagram"></use></svg><span class="tcu-visuallyhidden"><?php esc_html_e( 'Instagram', 'tcu_web_standards' ); ?></span></a>

							<a class="tcu-pinterest" href="https://www.pinterest.com/tcuedu/" title="The official TCU's Pinterest account"><svg focusable="false" height="30" width="30"><use xlink:href="#pinterest"></use></svg><span class="tcu-visuallyhidden"><?php esc_html_e( 'Pinterest', 'tcu_web_standards' ); ?></span></a>

							<a class="tcu-snapchat" href="<?php echo esc_url( get_template_directory_uri() ); ?>/library/images/snapcode.jpeg" title="The official TCU's Snapchat account"><svg focusable="false" height="30" width="30"><use xlink:href="#snapchat"></use></svg><span class="tcu-visuallyhidden"><?php esc_html_e( 'Snapchat', 'tcu_web_standards' ); ?></span></a>

							<a class="tcu-twitter" href="https://twitter.com/tcu" title="The official TCU's Twitter account"><svg focusable="false" height="30" width="30"><use xlink:href="#twitter"></use></svg><span class="tcu-visuallyhidden"><?php esc_html_e( 'Twitter', 'tcu_web_standards' ); ?></span></a>

							<a class="tcu-youtube" href="https://www.youtube.com/user/TCU" title="The official TCU's YouTube account"><svg focusable="false" height="30" width="30"><use xlink:href="#youtube"></use></svg><span class="tcu-visuallyhidden"><?php esc_html_e( 'YouTube', 'tcu_web_standards' ); ?></span></a>

						</div>

						<div class="unit m-size1of1 size1of2">
							Copyright &copy; <?php echo esc_html( date( 'Y' ), 'tcu_web_standards' ); ?> <a aria-label="Texas Christian University - Go back to the main www.tcu.edu homepage" href="http://www.tcu.edu"><?php esc_html_e( 'Texas Christian University', 'tcu_web_standards' ); ?></a>. <?php esc_html_e( 'All rights reserved.', 'tcu_web_standards' ); ?>
						</div>
					</div><!-- end of .group -->

				</div>

				<?php // Scroll to top. ?>
				<button type="button" class="tcu-top" title="Back to top"><span class="tcu-visuallyhidden"><?php esc_html_e( 'Top', 'tcu_web_standards' ); ?></span></button>

			</div><!-- end of .tcu-footer-main -->

		</footer><!-- end of .tcu-footer -->

	</div><!-- end of .tcu-layout-container -->


	<?php // All js scripts are loaded in library/tcu-core.php. ?>
	<?php wp_footer(); ?>

	</body>

</html>

