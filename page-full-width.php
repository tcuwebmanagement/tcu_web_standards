<?php
/**
 * Template Name: Full Width (no sidebar)
 *
 * This is the template that displays pages with no sidebar.
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 1.0.0
 */

// Let's make sure nobody can access this page directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php get_header(); ?>

<?php
// Display the breadcrumbs.
if ( function_exists( 'tcu_breadcrumbs_list' ) ) {
	tcu_breadcrumbs_list();
}
?>

<div class="tcu-layoutwrap--transparent">

	<div class="tcu-layout-constrain inner-content cf">

		<main class="unit size1of1 m-size1of1 tcu-below16 cf">

			<?php
			/**
			 * We add #main name anchor to our content element because we have a skip
			 * main navigation link for accessibility
			 */
			?>
			<a name="main" tabindex="-1" id="main"><span class="tcu-visuallyhidden"><?php esc_html_e( 'Main Content', 'tcu_web_standards' ); ?></span></a>

			<?php
			// Start the loop.
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();

					// Include the page template.
					get_template_part( 'partials/content', 'page' );
				endwhile;
			else :
					// Include the pagination template.
					get_template_part( 'partials/content', 'pagination' );
			endif;
			?>

		</main><!-- end of .unit -->

	</div><!-- end of .tcu-layout-constrain -->

</div><!-- end .tcu-layoutwrap--transparent -->

<?php get_footer(); ?>
