## TCU Web Standards Theme

TCU Web Standards theme to be used
for the university. This theme is in line with
the university's brand and web standards.

Author: Mayra Perales <mailto:m.j.perales@tcu.edu>

---

**Unreleased**

**4.10.0**

- Update Twitter icon
- Add icons for Threads and Linkedin

**4.9.6**

- Update outdated packages so that Gulpfile.js can run tasks
- Remove deregister of jQuery 

**4.9.5**

- Update footer utility links
- Remove SACSCOC statement text

**4.9.2**

- Change wp_register_script and wp_register_style to use theme version number

**4.9.1**

- Add all files in library/images to the gulp copy task

**4.9.0**

- Change menu expand elements to a button
- Add text label to expand button for mobile menu and desktop menu
- Add focus styles to mobile menu
- Remove dist folder, compiled css, and compiled js from repo
- Add a build task
- Remove Grunt from repo
- Run npm audit fix
- Update Author URI in style.css

**4.8.5**

- Update version number

**4.8.4**

- Remove link to legacy asp social media directory

**4.8.3**

- Add custom ID support to primary and secondary button short codes

**4.8.2**

- Remove pointer events on the up/down arrow of accordion headers

**4.8.1**

- Move the department widgets container inside of the footer
- Remove aria-hidden from items that are meant to be present for screen readers
- Place "Texas Christian University" at the beginning of aria-label in copyright statement link
- Add tabindex=-1 to the backToTop button to move focus to the top when clicked
- Add focusable="false" to SVG icons
- Fix skip link in IE by adding a name anchor above the main HTML element
- Added tabindex="-1" to the "I am looking for..." link in universal navigtaion
- Change function names in the admin section to avoid conflict with TCU Admin plugin
- Added an alert when TCU Admin plugin is installed while using this theme
- Underline the current page for accessibility
- When openning the sub-menu, we move focus to first item for accessibility
- Replaced background-images with inline images for the navigations, accordions and logo
- Open sub-menu if on current-page
- Removed sliding animation from buttons - those CSS classes are still available for backward compatability
- Styled theme for high contrast mode in IE

**4.8.0**

- Prevent menu.js from breaking when user accidently adds more than one level down in the dropdown menu
- Switched the order of the search form in the universal navigation
- Changed the width of the footer navigation to 100%
- Updated responsiveTabs.js to v1.6.3
- Added ESLint, SassLint, and PHPCS config files and fixed all issues/errors
- Renamed file names to use hyphens as word seperator
- Added DocBlocks to files
- Replaced Grunt with Gulp (We only use Grunt to compress our theme)
- Added classList polyfill for IE 9 support
- Fixed universal navigation in IE 9
- Fixed footer navigation in IE 9
- Fixed arrow icon for universal nav in IE 9
- Enqueue IE 8 specific css correctly in tcu-core.php
- Fixed IE 8 stylesheet, layout was completely broken
- Fixed arrows in accordions for IE 8-9 browsers. Arrows were not showing up
- Move social-icons and scroll up button into footer tag
- Added aria-labelledby to landmarks
- Updated tag links to be bold
- Updated comment links to bold
- Removed search form in search.php and 404.php
- Pages/Posts have the_title function
- Update archive title to h1
- Added missing text_domain to templates
- Changed widget headings to h5
- Changed blog titles to h2

**v4.7.0**

- Added accessibility link to footer menu
- Alphabetized the footer menu
- Updated the university address in footer
- Added better title description to the social media icons
- Added contrast to the footer widgets alert message
- Changed text color for .tcu-alert--info class to add contrast

**v4.6.1**

- Fixed Safari 9 menu issue
- Added support for the last 4 browser versions in autoprefixer

**v4.6**

- Corrected version number
- Added expand/contract buttons after the a link within the .menu-item-has-children li

**v4.5.12**

- The tcu.menu.js script supports multiple navigations in the sidebar
- Seperated all scripts instead of having one concatenated file
- Rewrote accordion script to support aria- labels
- Moved skip nav link to the top of the header
- Removed duplicate search role in universal navigation
- Added aria-labels for header and footer navigations
- Added focus styles into the tcu-button--primary class
- Removed width property and replaced with flex-basis
- Replaced the submit image with submit button
- Added focus styling to the submit button in search form
- Fixed the tcu_web_standards_check_updates function

**v4.4.12**

- Replaced forEach method with for loop in tcu.menu.js

**v4.4.11**

- Fixed a typo in the word animation for responsive tabs
- Added font-display
- Combined the dropdown.js and sidenav.js files
- Only one sub-menu opens at a time in the main navigation
- Sub-menus can be closed by pressing the ESC key
- Sub-menus can be closed by clicking outside into the body tag
- Added velocity.js to slowly stop using jQuery
- Scroll to top includes requestAnimationFrame for better performance
- PHP is now following WordPress code standards instead of PSR2 standards

**v4.4.10**

- Title tag was being added twice to the head tag
- Making sure our files can't be accessed directly
- Deleted tcu-help files like images/js

**v4.3.10**

- Bolded links inside tcu-modal
- underline links on hover inside of tcu-modal
- Autoprefixer added flagged CSS properties
- Removed black outline on primary button
- Added aria-label to Read More in blog archive template
- Changed the color to white for the mobile menu hamburger button
- Increased the opacity of the submenu for mobile

**v4.3.9**

- Fixed the visited state in the primary button class

**v4.3.8**

- Added aria-label option to button shortcode
- Focus outline goes around entire social icon
- Added spacing to the navigation to even out focus outline
- Added onclick event to hover menu in the univeral nav
- Visited state of navigation is secondary yellow

**v4.3.7**

- Incorporated different SVG social icons into the footer
- Added snapchat icon to footer
- Dropdowns are now active by a click action
- Styled tag cloud within the sidebar
- Updated the university address in footer
- Made updates to the :focus state and link elements for accessibility
- Added height and width to SVG icons

**v4.2.7**

- Removed multisite transient function that was causing an error

**v4.2.6**

- More secure way to update this theme

**v3.2.6**

- Removed paragraph issue within a shortcode
- Updated version number in CSS and JS 'wp_register_style' function

**v3.2.5**

- Added two and three column shortcode to editor
- Removed the TCU Help section

**v3.1.5**

- Fixed TCU Help Section
- Fixed tinyMCE script not loading

**v3.1.4**

- Removed .tcu-left-100 from .tcu-alert--info in comments.php file
- Added arvo to \_variables file
- Make department widgets conditional
- Added footer copy within \_e() function
- Add padding to mobile menu in the footer
- Changed the accordion title to an H4 and removed uppercase
- Header H1 changed font size to 2.5rem and removed uppercase
- Changed the size of the TCU Banner to 129px X 113px
- Changed H1 heading wrapper width to 60% for medium-screens
- Changed Footer widgets headings to h6 with .h4 class for styling
- Added comments to the main navigation about about the skip link
- Changed layout main body layout for tablets
- Combined JS for admin
- Hosting Google fonts locally for faster load time

**v3.1.3**

- adjusted heading margins in editor-styles.css
- styled alert messages in comment form
- added image dimensions for footer wordmark widget area
- fixed footer alignment on screens between 535px and 700px
- replaced accordion h3 to button tag
- styled search form inside main sidebar
- link focus color will be secondary-blue
- fixed apple-icon-touch link
- fixed footer menu not showing up on mobile
- fixed styling for long text in drop down menu
- fixed image sizing in tcu-modals
- styled grey background and ul ul in visual editor

**v3.1.2**

- page-full-width had typo in class name
- added space variables
- fixed empty links for accessibility
- fixed footer widgets for tablets
- move repeated code into template parts
- removed btm padding on .tcu-layoutwrap--transparent
- fixed table styling
- adjust other small styling issues
- adjusted line height for headings
- moved TCU banner to top left on mobile
- TCU banner is smaller on mobile
- moved sidebar to bottom of page on mobile
- left aligned thead
- added 60px 70px 80px font size to visual editor
- added support for taxonomy into breadcrumbs
- fixed mobile nav in IE11
- fixed accordion arrows in IE11
- fixed background color in footer
- fixed mobile menu top margin for ul ul

**v3.0.2**

- Keep theme settings when theme deactivates

**v3.0.1**

- Fixed spacing on dropdown menu
- Added conditional to comments function
- Footer social links are now text too
- Added background color to header
- Added background color to footer
- Added other accessibility elements
- fixed mobile nav going over wp admin menu on hover
- fixed double border on mobile nav
- fixed mobile menu hover color to white

**v3.0.0**

- fixed vertical align in icons within a button
- updated text on pagination fallback
- styled checkboxes/radio inputs behind .tcu-fieldset-inputs class
- updated universal nav with correct links
- removed footer navigation from wp dashboard
- updated footer navigation with correct links
- added a formidable class for better form support when using Formidable Plugin
- appended universal nav to .mean-nav
- updated grid and breakpoints to incorporate better tablet support
- fixed universal nav in IE
- added left margin to top logo on tablets
- removed the visit link on footer
- changed the universal bg color in mobie view

**v2.2.9**

- fixed accordion padding to clear icon
- added tcu-full-width class back
- styled current_page_item in sidenav
- changed subnav expand/contract to down/up arrows
- adjusted right padding on sub nav to clear arrow
- changed grid padding to 15px instead of a percentage
- added bottom padding to tab content area
- header title expands 79% on desktop
- sidenav contracts on mobile view
- fixed grid classes on footer widgets
- removed tcu-grid-filler class

**v2.1.9**

- specified text color for background color CSS classes
- added padding on css classes
- removed padding on universal nav that was added by CSS classes
- removed padding from nav that was added by CSS classes
- W3C error's fixed
- updated changelog file with latest updates
- fixed mobile search bg
- removed \_circles.scss file
- fixed header.php

**v2.0.9**

- page.php contained form template. Deleted that. It was a mistake!

**v2.0.8**

- fixed SVG fallbacks

**v2.0.7**

- max-text width was preventing full width on full width template
- add SVG fallback pngs

**v2.0.6**

- added a max-text width for content area
- styled radio/checked inputs
- styled dropdown inputs
- alphabetized CSS properties
- a lot of css clean up

**v2.0.5**

- updated theme name
- updated theme information
- updated theme tags
- updated theme license
- updated heading tags in scss files
- changed screenshot for theme

**v2.0.4**

- added better support for helper classes
- turned off comments template in single.php

**v2.0.3**

- Fixed universal nav icons

**v2.0.2**

- Fixed footer social media icons

**v2.0.1 update**

- moved jquery to the footer

**v2.0.0 update**

- removed mmenu JS file
- concat all JS files into one
- cleaned all references to "bones"
- clean up unnecessary code
- fixed wp_title
- fixed footer widget styling
- fixed HR styling
- update editor-styles to reflect new font options in the editor
- updated functions.php for better child theme support
- updated header svg's to pull from correct directory
- updated cf class
- fixed title tag
- fixed universal nav margins
- fixed margin-top and margin-bottom to 0 for better child theme support
- fixed margin on nav
- updated google fonts http request
- changed CSS class name to follow BEM naming rules
- combined TCU_Help and TCU_Admin plugins
- concat TCU Help JS files

**v1.1.0**

- Initial theme
- a lot of testing going on
