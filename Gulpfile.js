// Require our dependencies
const package = require('./package.json');
const autoprefixer = require( 'autoprefixer' );
const babel = require( 'gulp-babel' );
const browserSync = require( 'browser-sync' );
const cheerio = require( 'gulp-cheerio' );
const concat = require( 'gulp-concat' );
const cssnano = require( 'gulp-cssnano' );
const del = require( 'del' );
const eslint = require( 'gulp-eslint' );
const gulp = require( 'gulp' );
const imagemin = require( 'gulp-imagemin' );
const notify = require( 'gulp-notify' );
const plumber = require( 'gulp-plumber' );
const postcss = require( 'gulp-postcss' );
const rename = require( 'gulp-rename' );
const replace = require( 'gulp-replace' );
const sass = require( 'gulp-sass' )( require( 'sass' ) );
const sassdoc = require( 'sassdoc' );
const sassLint = require( 'gulp-sass-lint' );
const sourcemaps = require( 'gulp-sourcemaps' );
const spritesmith = require( 'gulp.spritesmith' );
const svgmin = require( 'gulp-svgmin' );
const svgstore = require( 'gulp-svgstore' );
const uglify = require( 'gulp-uglify' );
const zip = require( 'gulp-zip' );

let siteName = 'tcu-web-standards'; // set your siteName here

// Set assets paths.
const paths = {
    css: [ 'library/css/*.css', '!library/css/*.min.css' ],
    images: [
        'library/images/**',
        '!library/images/svg/sprite.symbol.svg',
        '!library/images/svg/sprite.symbol-older-beforev4.2.7.svg'
    ],
    php: [ './*.php', './**/*.php' ],
    sass: 'library/scss/**/*.scss',
    compiledjs: 'library/js/compiled/*.js',
    scripts: [ 'library/js/*.js', '!library/js/*.min.js' ],
    sprites: 'library/images/*.png'
};

/**
 * Handle errors and alert the user.
 */
function handleErrors() {
    const args = Array.prototype.slice.call( arguments );

    notify
        .onError( {
            title: 'Task Failed [<%= error.message %>',
            message: 'See console.',
            sound: 'Sosumi' // See: https://github.com/mikaelbr/node-notifier#all-notification-options-with-their-defaults
        } )
        .apply( this, args );

    // Prevent the 'watch' task from stopping.
    this.emit( 'end' );
}

/**
 * Delete style.css and style.min.css before we minify and optimize
 */
gulp.task( 'clean:styles', () => del( [ 'library/css/style.css', 'library/css/style.min.css' ] ) );

/**
 * Compile Sass and run stylesheet through PostCSS.
 *
 * https://www.npmjs.com/package/gulp-sass
 * https://www.npmjs.com/package/gulp-postcss
 * https://www.npmjs.com/package/gulp-autoprefixer
 * https://www.npmjs.com/package/css-mqpacker
 */
gulp.task(
    'postcss',
    gulp.series( 'clean:styles', () =>
        gulp
            .src( 'library/scss/*.scss', paths.css )

            // Deal with errors.
            .pipe( plumber( { errorHandler: handleErrors } ) )

            // Wrap tasks in a sourcemap.
            .pipe( sourcemaps.init() )

            // Compile Sass using LibSass.
            .pipe(
                sass( {
                    errLogToConsole: true,
                    outputStyle: 'expanded' // Options: nested, expanded, compact, compressed
                } )
            )

            // Parse with PostCSS plugins.
            .pipe(
                postcss( [ autoprefixer ] )
            )

            // Create sourcemap.
            .pipe( sourcemaps.write() )

            // Create style.css.
            .pipe( gulp.dest( 'library/css/' ) )
            .pipe( browserSync.stream() )
    )
);

/**
 * Minify and optimize style.css.
 *
 * https://www.npmjs.com/package/gulp-cssnano
 */
gulp.task(
    'cssnano',
    gulp.series( 'postcss', () =>
        gulp
            .src( 'library/css/style.css' )
            .pipe( plumber( { errorHandler: handleErrors } ) )
            .pipe(
                cssnano( {
                    safe: true, // Use safe optimizations.
                    autoprefixer: false
                } )
            )
            .pipe( rename( 'style.min.css' ) )
            .pipe( gulp.dest( 'library/css/' ) )
            .pipe( browserSync.stream() )
    )
);

/**
 * Minify and optimize ie.css.
 *
 * https://www.npmjs.com/package/gulp-cssnano
 */
gulp.task(
    'cssie',
    gulp.series( 'postcss', () =>
        gulp
            .src( 'library/css/ie.css' )
            .pipe( plumber( { errorHandler: handleErrors } ) )
            .pipe(
                cssnano( {
                    safe: true // Use safe optimizations.
                } )
            )
            .pipe( rename( 'ie.min.css' ) )
            .pipe( gulp.dest( 'library/css/' ) )
            .pipe( browserSync.stream() )
    )
);

/**
 * Delete the svg-icons.svg before we minify, concat.
 */
gulp.task( 'clean:icons', () => del( [ 'library/images/svg-icons.svg' ] ) );

/**
 * Minify, concatenate, and clean SVG icons.
 *
 * https://www.npmjs.com/package/gulp-svgmin
 * https://www.npmjs.com/package/gulp-svgstore
 * https://www.npmjs.com/package/gulp-cheerio
 */
gulp.task(
    'svg',
    gulp.series( 'clean:icons', () =>
        gulp
            .src( 'svg_out/original_svgs/tcu-icon-library-svg/*.svg' )

            // Deal with errors.
            .pipe( plumber( { errorHandler: handleErrors } ) )

            // Minify SVGs.
            .pipe( svgmin() )

            // Add a prefix to SVG IDs.
            .pipe( rename( { prefix: 'icon-' } ) )

            // Combine all SVGs into a single <symbol>
            .pipe( svgstore( { inlineSvg: true } ) )

            // Clean up the <symbol> by removing the following cruft...
            .pipe(
                cheerio( {
                    run: function( $, file ) {
                        $( 'svg' ).attr( 'style', 'display:none' );
                        $( '[fill]' ).removeAttr( 'fill' );
                        $( 'path' ).removeAttr( 'class' );
                        $( 'title' ).remove();
                    },
                    parserOptions: { xmlMode: true }
                } )
            )

            // Save svg-icons.svg.
            .pipe( gulp.dest( 'library/images/svg/' ) )
            .pipe( browserSync.stream() )
    )
);

/**
 * Delete the images before we minify.
 */
gulp.task( 'clean:images', () => del( [ 'dist/library/images/*' ] ) );

/**
 * Optimize images.
 *
 * https://www.npmjs.com/package/gulp-imagemin
 */
gulp.task( 'imagemin', () =>
    gulp
        .src( paths.images )
        .pipe( plumber( { errorHandler: handleErrors } ) )
        .pipe(
            imagemin( {
                optimizationLevel: 5,
                progressive: true,
                interlaced: true,
                svgoPlugins: [
                    {
                        removeViewBox: false,
                        removeAttrs: { attrs: [ 'xmlns' ] }
                    }
                ]
            } )
        )
        .pipe( gulp.dest( 'dist/library/images' ) )
);

/**
 * Delete the sprites.png before rebuilding sprite.
 */
gulp.task( 'clean:sprites', () => {
    del( [ 'library/images/sprites.png' ] );
} );

/**
 * Concatenate images into a single PNG sprite.
 *
 * https://www.npmjs.com/package/gulp.spritesmith
 */
gulp.task( 'spritesmith', () =>
    gulp
        .src( paths.sprites )
        .pipe( plumber( { errorHandler: handleErrors } ) )
        .pipe(
            spritesmith( {
                imgName: 'sprites.png',
                cssName: 'scss/core/_sprites.scss',
                imgPath: 'library/images/sprites/sprites.png',
                algorithm: 'binary-tree'
            } )
        )
        .pipe( gulp.dest( 'dist/library/images/sprites/' ) )
        .pipe( browserSync.stream() )
);

/**
 * Delete scripts before we compile our scripts
 */
gulp.task( 'clean:scripts', () => del( [ 'library/js/compiled/*.js' ] ) );

/**
 *
 * Use babel for our scripts
 *
 * https://www.npmjs.com/package/gulp-babel
 */
gulp.task( 'babel', () =>
    gulp
        .src( paths.scripts )

        // Convert ES6+ to ES2015.
        .pipe(
            babel( {
                presets: [
                    [
                        'env',
                        {
                            targets: {
                                browsers: [ '> 2%', 'last 4 versions', 'Safari 8', 'IE 9', 'IE 8' ]
                            }
                        }
                    ]
                ]
            } )
        )
        .pipe( gulp.dest( 'library/js/compiled/' ) )
);

/**
 * Concatenate and transform JavaScript.
 * Old way of doing things.
 *
 * https://www.npmjs.com/package/gulp-concat
 * https://github.com/babel/gulp-babel
 * https://www.npmjs.com/package/gulp-sourcemaps
 */
gulp.task( 'concat', () =>
    gulp
        .src( paths.scripts )

        // Deal with errors.
        .pipe( plumber( { errorHandler: handleErrors } ) )

        // Start a sourcemap.
        .pipe( sourcemaps.init() )

        // Convert ES6+ to ES2015.
        .pipe(
            babel( {
                presets: [
                    [
                        'env',
                        {
                            targets: {
                                browsers: [ 'last 4 versions' ]
                            }
                        }
                    ]
                ]
            } )
        )

        // Concatenate partials into a single script.
        .pipe( concat( 'all-tcu-scripts.js' ) )

        // Append the sourcemap to all-tcu-scripts.js.
        .pipe( sourcemaps.write() )

        // Save all-tcu-scripts.js
        .pipe( gulp.dest( 'library/js/concat' ) )
        .pipe( browserSync.stream() )
);

/**
 * Minify compiled JavaScript.
 *
 * https://www.npmjs.com/package/gulp-uglify
 */
gulp.task( 'uglify', () =>
    gulp
        .src( paths.compiledjs )
        .pipe( plumber( { errorHandler: handleErrors } ) )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe(
            babel( {
                presets: [
                    [
                        'env',
                        {
                            targets: {
                                browsers: [ 'last 4 versions' ]
                            }
                        }
                    ]
                ]
            } )
        )
        .pipe(
            uglify( {
                mangle: false
            } )
        )
        .pipe( gulp.dest( 'library/js/min' ) )
);

/**
 * Sass linting.
 *
 * https://www.npmjs.com/package/sass-lint
 */
gulp.task( 'sass:lint', () =>
    gulp
        .src( [
            'library/scss/**/*.scss',
            '!library/scss/core/_normalize.scss',
            '!library/scss/core/_sprites.scss',
            '!node_modules/**'
        ] )
        .pipe( sassLint() )
        .pipe( sassLint.format() )
        .pipe( sassLint.failOnError() )
);

/**
 * JavaScript linting.
 *
 * https://www.npmjs.com/package/gulp-eslint
 */
gulp.task( 'js:lint', () =>
    gulp
        .src( [
            'library/js/*.js',
            'library/js/compiled/*.js',
            '!dist/library/js/**.js',
            '!library/js/min/*.min.js',
            '!Gruntfile.js',
            '!Gulpfile.js',
            '!node_modules/**'
        ] )
        .pipe( eslint() )
        .pipe( eslint.format() )
        .pipe( eslint.failAfterError() )
);

/**
 * Sass docs.
 *
 * http://sassdoc.com/getting-started/
 */
gulp.task( 'sassdoc', function() {
    let options = {
        dest: 'docs',
        verbose: true
    };

    return gulp.src( 'library/scss/**/*.scss' ).pipe( sassdoc( options ) );
} );

/**
 * Delete contents in dist/ directory before we initialize a fresh copy.
 */
gulp.task( 'clean:dist', () => del( [ 'dist/*', '!dist/' ] ) );

/**
 * Copy necessary files for a clean WP theme
 *
 * Removes Gruntfile, Gulpfile, package.json and other unwanted files
 *
 */
gulp.task(
    'copy',
    gulp.series( 'clean:dist', () =>
        gulp
            .src(
                [
                    './*.php',
                    './*.ico',
                    './*.css',
                    './CHANGELOG.md',
                    './screenshot.png',
                    './favicon.png',
                    './favicon.ico',
                    './partials/*.php',
                    './library/**/*.php',
                    './library/css/**.css',
                    './library/css/fonts/**',
                    './library/css/!*.map',
                    './library/js/**',
                    './library/translation/**',
                    './library/scss/**/*.scss',
                    './library/images/**'
                ],
                { base: '.' }
            )
            .pipe( gulp.dest( 'dist/' ) )
    )
);

/**
 * Update version number in dist style.css
 *
 * Replaces the "Version :" line with the package.json version number
 *
 */
gulp.task('update-version', () =>
  gulp.src('dist/style.css')
    .pipe(replace(/Version: (.*?)\n/g, `Version: ${package.version}\n`))
    .pipe(gulp.dest('./'))
);

/**
 * Zip up our /dist (production wordpress theme)
 *
 */
gulp.task('zip', () =>
  gulp.src('dist/**')
    .pipe(rename(function (path) {
      path.dirname = 'tcu_web_standards/' + path.dirname;
    }))
    // zip the theme
    .pipe(zip(`tcu_web_standards.${package.version}.zip`))
    // move theme to root of project
    .pipe(gulp.dest('./'))
);

/**
 * Process tasks and reload browsers on file changes.
 *
 * https://www.npmjs.com/package/browser-sync
 */
gulp.task( 'watch', function() {

    // Kick off BrowserSync.
    browserSync( {
        open: false, // Open project in a new tab?
        injectChanges: true, // Auto inject changes instead of full reload.
        proxy: 'http://' + siteName + '.test', // Use your localhost path to use BrowserSync - Note: using Valet here.
        host: siteName + '.test',
        watchOptions: {
            debounceDelay: 1000 // Wait 1 second before injecting.
        }
    } );

    gulp.watch( paths.php ).on( 'change', browserSync.reload );
    gulp.watch( paths.sass, gulp.series( 'styles' ) );
    gulp.watch( paths.scripts, gulp.series( 'scripts' ) );
} );

/**
 * Create individual tasks.
 */
gulp.task( 'icons', gulp.series( 'svg' ) );
gulp.task( 'scripts', gulp.series( 'babel', 'uglify' ) );
gulp.task( 'styles', gulp.series( 'postcss', 'cssie', 'cssnano' ) );
gulp.task( 'sprites', gulp.series( 'spritesmith' ) );
gulp.task( 'lint', gulp.series( 'sass:lint', 'js:lint' ) );
gulp.task( 'build', gulp.series(gulp.parallel('styles', 'scripts', 'imagemin'), 'copy', 'update-version', 'zip'));
gulp.task( 'default', gulp.series( 'styles', 'scripts', 'imagemin' ) );
