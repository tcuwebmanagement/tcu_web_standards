<?php
/**
 * The template to display the search results.
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 1.0.0
 */

// Let's make sure nobody can access this page directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php get_header(); ?>

<div class="tcu-layoutwrap--transparent">

	<div class="tcu-layout-constrain cf">

		<main class="unit size1of1 m-size1of1 cf">

			<?php
			/**
			 * We add #main name anchor to our content element because we have a skip
			 * main navigation link for accessibility
			 */
			?>
			<a name="main" tabindex="-1" id="main"><span class="tcu-visuallyhidden"><?php esc_html_e( 'Main Content', 'tcu_web_standards' ); ?></span></a>

			<h1 class="archive-title"><span><?php esc_html_e( 'Search Results for:', 'tcu_web_standards' ); ?></span> <?php echo esc_attr( get_search_query() ); ?></h1>

			<?php
			// Start the loop.
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();

					// Include the page template.
					get_template_part( 'partials/content', 'search' );
				endwhile;

				// Include the pagination template.
				get_template_part( 'partials/content', 'pagination' );
			else :
				// Include the search-none template.
				get_template_part( 'partials/search', 'none' );
			endif;
			?>

		</main><!-- end of .unit -->

	</div><!-- end of .tcu-layout-constrain -->

</div><!-- end .tcu-layoutwrap--transparent -->

<?php get_footer(); ?>
