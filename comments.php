<?php
/**
 * The template to display comments
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 4.8.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}

?>

<?php if ( have_comments() ) : ?>
	<h4><?php comments_number( __( '<span>No</span> Responses', 'tcu_web_standards' ), __( '<span>One</span> Response', 'tcu_web_standards' ), _n( '<span>%</span> Response', '<span>%</span> Responses', get_comments_number(), 'tcu_web_standards' ) ); ?> to &#8220;<?php the_title(); ?>&#8221;</h4>

	<nav aria-label="Comments Navigation Top" class="tcu-comments__nav cf">
		<ul class="cf">
			<li><?php previous_comments_link(); ?></li>
			<li><?php next_comments_link(); ?></li>
		</ul>
	</nav>

	<ol class="tcu-comments__list">
		<?php wp_list_comments( 'type=comment&callback=tcu_comments' ); ?>
	</ol>

	<nav  aria-label="Comments Navigation Bottom" class="tcu-comments__nav cf">
		<ul class="cf">
			<li><?php previous_comments_link(); ?></li>
			<li><?php next_comments_link(); ?></li>
		</ul>
	</nav>

	<?php else : // This is displayed if there are no comments so far. ?>

	<?php if ( comments_open() ) : ?>
	<?php // If comments are open, but there are no comments. ?>

	<?php else : // Comments are closed. ?>

	<?php // If comments are closed. ?>
	<!--p class="nocomments"><?php esc_html_e( 'Comments are closed.', 'tcu_web_standards' ); ?></p-->

	<?php endif; ?>

<?php endif; ?>


<?php if ( comments_open() ) : ?>

<section aria-label="Form to submit a comment" id="respond" class="tcu-comments__respond-form">

	<?php /* translators: $s: author */ ?>
	<h3><?php comment_form_title( __( 'Leave a Reply', 'tcu_web_standards' ), __( 'Leave a Reply to %s', 'tcu_web_standards' ) ); ?></h3>

	<div id="tcu-comments__cancel">
		<p class="small"><?php cancel_comment_reply_link(); ?></p>
	</div>

	<?php if ( get_option( 'comment_registration' ) && ! is_user_logged_in() ) : ?>
		<div class="tcu-alert tcu-alert--help tcu-left-100">
			<p><?php esc_html_e( 'You must be', 'tcu_web_standards' ); ?> <a href="<?php echo esc_url( wp_login_url( get_permalink() ) ); ?>"><?php esc_html_e( 'logged in', 'tcu_web_standards' ); ?></a> <?php esc_html_e( 'to post a comment.', 'tcu_web_standards' ); ?></p>
		</div>
	<?php else : ?>

	<form action="<?php echo esc_url( get_option( 'siteurl' ) ); ?>/wp-comments-post.php" method="post" id="commentform">

	<?php if ( is_user_logged_in() ) : ?>

		<p class="tcu-comments__logged-in-as">
			<?php esc_html_e( 'Logged in as', 'tcu_web_standards' ); ?>
				<a href="<?php echo esc_url( get_option( 'siteurl' ) ); ?>/wp-admin/profile.php">
					<?php echo esc_html( $user_identity ); ?>
				</a>.
				<a href="<?php echo esc_url( wp_logout_url( get_permalink() ) ); ?>" title="<?php esc_attr_e( 'Log out of this account', 'tcu_web_standards' ); ?>">
					<?php esc_html_e( 'Log out', 'tcu_web_standards' ); ?> <?php esc_html_e( '&raquo;', 'tcu_web_standards' ); ?>
				</a>
		</p>

	<?php else : ?>

	<ul class="tcu-comments__form cf" class="cf">

		<li class="m-size1of1 size1of2">
			<label for="author"><?php esc_html_e( 'Name', 'tcu_web_standards' ); ?>
			<?php
			if ( $req ) {
				esc_html_e( '(required)', 'tcu_web_standards' );
			}
			?>
		</label>
			<input type="text" name="author" id="author" value="<?php echo esc_attr( $comment_author ); ?>" placeholder="<?php esc_html_e( 'Your Name*', 'tcu_web_standards' ); ?>"
			<?php
			if ( $req ) {
				echo "aria-required='true'";
			}
			?>
			/>
		</li>

		<li class="m-size1of1 size1of2">
			<label for="email"><?php esc_html_e( 'E-Mail', 'tcu_web_standards' ); ?> <?php
			if ( $req ) {
				esc_html_e( '(required)', 'tcu_web_standards' );
			}
			?>
			</label>
			<input type="email" name="email" id="email" value="<?php echo esc_attr( $comment_author_email ); ?>" placeholder="<?php esc_html_e( 'Your E-Mail*', 'tcu_web_standards' ); ?>"
			<?php
			if ( $req ) {
				echo "aria-required='true'";
			}
			?>
			/>
			<small><?php esc_html_e( '(will not be published)', 'tcu_web_standards' ); ?></small>
		</li>

	</ul>

	<?php endif; ?>

	<p><label for="comment"><?php esc_html_e( 'Comments', 'tcu_web_standards' ); ?></label>
	<textarea name="comment" id="comment" placeholder="<?php esc_attr_e( 'Your Comment here...', 'tcu_web_standards' ); ?>"></textarea></p>

	<p class="g-recaptcha" data-sitekey="6LcghAsTAAAAABjb4whdF2prs95PK_jvjLWYUPlb"></p>

	<p>
		<input name="submit" type="submit" id="submit" class="button" value="<?php esc_attr_e( 'Submit', 'tcu_web_standards' ); ?>" />
		<?php comment_id_fields(); ?>
	</p>

	<div class="tcu-alert tcu-alert--info">
		<p id="allowed_tags" class="small"><strong>XHTML:</strong> <?php esc_html_e( 'You can use these tags', 'tcu_web_standards' ); ?>: <code><?php echo allowed_tags(); ?></code></p>
	</div>

	<?php do_action( 'tcu_comment_form', $post->ID ); ?>

	</form>

	<?php endif; // If registration required and not logged in. ?>
</section>

<?php endif; // if you delete this the sky will fall on your head. ?>
