<?php
/**
 *  The template for displaying all single posts and attachments
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 1.0.0
 */

// Let's make sure nobody can access this page directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php get_header(); ?>

<?php
// Display the breadcrumbs.
if ( function_exists( 'tcu_breadcrumbs_list' ) ) {
	tcu_breadcrumbs_list();
}
?>

<div class="tcu-layoutwrap--transparent">

	<div class="tcu-layout-constrain cf">

		<main class="unit size2of3 m-size2of3 cf">

			<?php
			/**
			 * We add #main name anchor to our content element because we have a skip
			 * main navigation link for accessibility
			 */
			?>
			<a name="main" tabindex="-1" id="main"><span class="tcu-visuallyhidden"><?php esc_html_e( 'Main Content', 'tcu_web_standards' ); ?></span></a>

			<?php
			// Start the loop.
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();

					// Include the single-post template.
					get_template_part( 'partials/content', 'single' );
				endwhile;
			else :
				// Get pagination template.
				get_template_part( 'partials/content', 'pagination' );
			endif;
			?>

		</main><!-- end of .unit -->

		<?php get_sidebar(); ?>

</div><!-- end of .tcu-layout-constrain -->

<?php get_template_part( 'partials/content', 'comments' ); ?>

</div><!-- end .tcu-layoutwrap--transparent -->

<?php get_footer(); ?>
