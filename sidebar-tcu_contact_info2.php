<?php
/**
 * The template for displaying the tcu_contact_info-2 widget in footer
 * No longer used because the 'id' of the registered sidebar has changed.
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 1.0.0
 *
 * @deprecated 4.7.0 No longer used by internal code and not recommended.
 * @see filename sidebar-tcu-contact-info-2.php
 */

?>
<div class="tcu-footer-departments__contact-info size1of3 m-size1of2 unit">

	<?php if ( is_active_sidebar( 'tcu_contact_info-2' ) ) : ?>

		<?php dynamic_sidebar( 'tcu_contact_info-2' ); ?>

	<?php else : ?>

		<?php // This content shows up if there are no widgets defined in the backend. ?>

		<div class="tcu-alert tcu-alert--help">
			<p><?php __( 'Please add more contact information here.', 'tcu_web_standards' ); ?></p>
		</div>

	<?php endif; ?>

</div><!-- end of .tcu-footer-departments__contact-info -->
