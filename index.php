<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme and one of
 * the two required files for a theme. It is used to display a page when nothing more
 * specific matches a query.
 *
 * @package tcu_web_standards
 * @since 1.0.0
 */

// Let's make sure nobody can access this page directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php get_header(); ?>

<?php
// Display the breadcrumbs.
if ( function_exists( 'tcu_breadcrumbs_list' ) ) {
	tcu_breadcrumbs_list();
}
?>

<div class="tcu-layoutwrap--transparent">

	<div class="tcu-layout-constrain cf">

		<main class="unit size2of3 m-size2of3 cf">

			<?php
			/**
			 * We add #main name anchor to our content element because we have a skip
			 * main navigation link for accessibility
			 */
			?>
			<a name="main" tabindex="-1" id="main"><span class="tcu-visuallyhidden"><?php esc_html_e( 'Main Content', 'tcu_web_standards' ); ?></span></a>

			<?php if ( is_home() && ! is_front_page() ) : ?>
				<h1><?php single_post_title(); ?></h1>
			<?php endif; ?>

			<?php
			// Start the loop.
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					?>

				<article aria-labelledby="blog-title-<?php the_ID(); ?>" id="post-<?php the_ID(); ?>" <?php post_class( 'tcu-article tcu-below16 cf' ); ?>>

					<?php get_template_part( 'partials/content', 'loop' ); ?>

				</article><!-- end of .tcu-article -->

				<?php
				endwhile;

				// Include the pagination template.
				get_template_part( 'partials/content', 'pagination' );
			else :

				// Include the content-none template.
				get_template_part( 'partials/content', 'none' );
			endif;
			?>

		</main><!-- end of .unit -->

		<?php get_sidebar(); ?>

	</div><!-- end of .tcu-layout-constrain -->

</div><!-- end .tcu-layoutwrap--transparent -->

<?php get_footer(); ?>
