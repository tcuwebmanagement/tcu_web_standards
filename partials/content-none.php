<?php
/**
 * The template for displaying a message that posts cannot be found
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 3.1.2
 */

?>

<article aria-labelledby="content-none" class="tcu-article hentry cf">

	<header class="tcu-article__header">
		<h1 id="content-none"><?php esc_html_e( 'Oops, Post Not Found!', 'tcu_web_standards' ); ?></h1>
	</header>

	<div class="tcu-article__content">
		<p><?php esc_html_e( 'Uh Oh. Something is missing. Try double checking things.', 'tcu_web_standards' ); ?></p>
	</div>

</article>
