<?php
/**
 * Template to display content on a page
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 3.1.2
 */

?>
<article aria-labelledby="post-<?php the_ID(); ?>" <?php post_class( 'tcu-article cf' ); ?>>

	<div class="tcu-article__content cf">
		<h1 id="post-<?php the_ID(); ?>"><?php the_title(); ?></h1>
		<?php the_content(); ?>
	</div>

</article><!-- end of .tcu-article -->
