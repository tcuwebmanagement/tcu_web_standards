<?php
/**
 * The template for displaying a message that no search results were found
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 3.1.2
 */

?>
<article aria-labelledby="no-search-results" class="tcu-article hentry cf">

	<header class="tcu-article__header">
		<h2 id="no-search-results"><?php esc_html_e( 'Sorry, No Results.', 'tcu_web_standards' ); ?></h2>
	</header>

	<div class="tcu-article__content">
		<p><?php esc_html_e( 'Try your search again.', 'tcu_web_standards' ); ?></p>
	</div>

</article>
