<?php
/**
 * Template for the single posts
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 3.1.2
 */

?>
<article aria-labelledby="post-<?php the_ID(); ?>" <?php post_class( 'tcu-article cf' ); ?>>

	<header class="tcu-article__header">

		<h1 id="post-<?php the_ID(); ?>" class="tcu-mar-b0" itemprop="headline"><?php the_title(); ?></h1>

		<p class="tcu-byline">
			<span>
				<time class="updated entry-time" datetime="<?php echo esc_attr( get_the_time( 'Y-m-d' ) ); ?>" itemprop="datePublished"><?php echo esc_html( get_the_time( get_option( 'date_format' ) ) ); ?></time>
			</span> / <span class="entry-author author" itemprop="author" itemscope itemptype="http://schema.org/Person"><?php the_author(); ?> </span> /
			<span>
				<?php
				printf(
					/* translators: 1: number of comments, 2: post title */
					_nx(
						'%1$s Comment',
						'%1$s Comments',
						get_comments_number(),
						'comments title',
						'tcu_web_standards'
					),
					number_format_i18n( get_comments_number() )
				);
				?>
			</span>
		</p>

	</header>

	<div class="tcu-article__content cf">
		<?php the_content(); ?>
	</div>

	<?php if ( has_tag() ) : ?>
		<footer class="tcu-article__footer">
			<?php the_tags( '<p class="tags"><span class="tags-title">' . __( 'Tags:', 'tcu_web_standards' ) . '</span> ', ', ', '</p>' ); ?>
		</footer>
	<?php endif; ?>

</article><!-- end of .tcu-article -->
