<?php
/**
 * The template to display the archive content
 *
 * @package tcu_web_standards
 * @since TCU Web Standrds 3.1.2
 */

?>
<article aria-labelledby="post-<?php the_ID(); ?>" <?php post_class( 'tcu-article below16 cf' ); ?>>

	<header class="tcu-article__header">
		<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
	</header>

	<div class="tcu-article__content cf">
		<span class="tcu-article__thumb"><?php the_post_thumbnail(); ?></span>
		<?php the_excerpt(); ?>
	</div>

</article><!-- end of .tcu-article -->
