<?php
/**
 * The template to display the content within the index loop
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 3.1.2
 */

?>
<header class="tcu-article__header">

	<h2 class="h3" id="blog-title-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
	<p class="tcu-byline">
		<span>
			<time class="updated entry-time" datetime="<?php echo esc_attr( get_the_time( 'Y-m-d' ) ); ?>" itemprop="datePublished"><?php echo esc_html( get_the_time( get_option( 'date_format' ) ) ); ?></time>
		</span> / <span class="entry-author author" itemprop="author" itemscope itemptype="http://schema.org/Person"><?php the_author(); ?> </span> /
		<span>
			<?php
			printf(
				/* translators: 1: number of comments, 2: post title */
				_nx(
					'%1$s Comment',
					'%1$s Comments',
					get_comments_number(),
					'comments title',
					'tcu_web_standards'
				),
				number_format_i18n( get_comments_number() )
			);
			?>
		</span>
	</p>

</header><!-- end of .article-header -->

<div class="tcu-article__content cf">
	<span class="tcu-article__thumb"><?php the_post_thumbnail(); ?></span>
	<?php the_excerpt(); ?>
</div>

<?php if ( has_tag() ) : ?>
	<footer class="tcu-article__footer">
		<p class="tags"><?php the_tags( '<span class="tags-title">' . __( 'Tags:', 'tcu_web_standards' ) . '</span> ', ', ', '' ); ?></p>
	</footer>
<?php endif; ?>
