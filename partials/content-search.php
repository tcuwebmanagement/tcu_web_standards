<?php
/**
 * The template to display the search loop
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 3.1.2
 */

?>
<article aria-labelledby="post-<?php the_ID(); ?>" <?php post_class( 'tcu-article tcu-below30 cf' ); ?>>

	<header class="tcu-article__header">
		<h1 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
	</header>

	<div class="tcu-article__content">
		<?php the_excerpt( '<span class="read-more">' . __( 'Read more &raquo;', 'tcu_web_standards' ) . '</span>' ); ?>
	</div>

</article><!-- end of .tcu-article -->
