<?php
/**
 * The template to display the 404 content
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 3.1.2
 */

?>
<article id="content-error" class="tcu-article hentry cf">

	<header class="tcu-article__header">
		<h1 class="tcu-mar-b0 tcu-moonface"><?php esc_html_e( 'Riff Ram Bah Zoo', 'tcu_web_standards' ); ?></h1>
		<h2 id="content-error" class="tcu-mar-t0"><?php esc_html_e( 'Looks like we lost you...', 'tcu_web_standards' ); ?></h2>
	</header>

	<div class="tcu-article__content">
		<div class="tcu-layout-center alignc">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/library/images/horned-frog.jpg" alt="TCU Horned Frog">
			<p class="h1 tcu-moonface tcu-alignc tcu-mar-t0"><?php esc_html_e( 'You found SuperFrog!', 'tcu_web_standards' ); ?></p>
		</div>

		<p><?php esc_html_e( 'SuperFrog is very happy that you came to visit, however, he knows you were not looking for him, you were looking for another page. Are you lost? The page you are trying to visit might be an old link or maybe it moved.', 'tcu_web_standards' ); ?></p>
	</div>

</article><!-- end of .tcu-article -->
