<?php
/**
 * The template for displaying the pagination
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 3.1.2
 */

?>

<?php if ( function_exists( 'tcu_page_navi' ) ) { ?>

	<?php tcu_page_navi(); ?>

<?php

} else {
?>

	<nav aria-label="pagination" class="wp-prev-next">

		<ul class="cf">
			<li class="prev-link"><?php next_posts_link( __( '&laquo; Previous Page', 'tcu_web_standards' ) ); ?></li>
			<li class="next-link"><?php previous_posts_link( __( 'Next &raquo;', 'tcu_web_standards' ) ); ?></li>
		</ul>

	</nav>

<?php } ?>
