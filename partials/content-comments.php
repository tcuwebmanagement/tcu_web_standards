<?php
/**
 * The template to display the comments
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 3.1.2
 */

?>
<?php
// If comments are open or we have at least one comment, load up the comment template.
if ( comments_open() || get_comments_number() ) :
?>

	<section aria-labelledby="tcu-comments" class="tcu-layout-constrain">

		<div class="tcu-comments cf">
			<h3 id="tcu-comments" class="tcu-comments__title"><?php esc_html_e( 'Comments', 'tcu_web_standards' ); ?></h3>
			<?php comments_template(); ?>
		</div>

	</section>

<?php endif; ?>
