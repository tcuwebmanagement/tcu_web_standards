/*eslint wrap-iife: [2, "inside"]*/

/*!
* jQuery meanMenu v2.0.8
* @Copyright (C) 2012-2014 Chris Wharton @ MeanThemes (https://github.com/meanthemes/meanMenu)
*
*/
/*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "AS IS," AND COPYRIGHT
* HOLDERS MAKE NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED,
* INCLUDING BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY OR
* FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF THE SOFTWARE
* OR DOCUMENTATION WILL NOT INFRINGE ANY THIRD PARTY PATENTS,
* COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.COPYRIGHT HOLDERS WILL NOT
* BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL
* DAMAGES ARISING OUT OF ANY USE OF THE SOFTWARE OR DOCUMENTATION.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://gnu.org/licenses/>.
*
* Find more information at http://www.meanthemes.com/plugins/meanmenu/
*
*/
( function( $ ) {
    'use strict';
    $.fn.meanmenu = function( options ) {
        var defaults = {
            meanMenuTarget: jQuery( this ), // Target the current HTML markup you wish to replace
            meanMenuContainer: 'body', // Choose where meanmenu will be placed within the HTML
            meanMenuClose: 'X', // single character you want to represent the close menu button
            meanMenuCloseSize: '18px', // set font size of close button
            meanMenuOpen: '<span /><span /><span />', // text/markup you want when menu is closed
            meanRevealPosition: 'right', // left right or center positions
            meanRevealPositionDistance: '0', // Tweak the position of the menu
            meanRevealColour: '', // override CSS colours for the reveal background
            meanScreenWidth: '480', // set the screen width you want meanmenu to kick in at
            meanNavPush: '', // set a height here in px, em or % if you want to budge your layout now the navigation is missing.
            meanShowChildren: true, // true to show children in the menu, false to hide them
            meanExpandableChildren: true, // true to allow expand/collapse children
            meanExpand: '+', // single character you want to represent the expand for ULs
            meanContract: '-', // single character you want to represent the contract for ULs
            meanRemoveAttrs: false, // true to remove classes and IDs, false to keep them
            onePage: false, // set to true for one page sites
            meanDisplay: 'block', // override display method for table cell based layouts e.g. table-cell
            removeElements: '' // set to hide page elements
        };
        options = $.extend( defaults, options );

        // get browser width
        var currentWidth = window.innerWidth || document.documentElement.clientWidth;

        return this.each( function() {
            var meanMenu = options.meanMenuTarget;
            var meanContainer = options.meanMenuContainer;
            var meanMenuClose = options.meanMenuClose;
            var meanMenuCloseSize = options.meanMenuCloseSize;
            var meanMenuOpen = options.meanMenuOpen;
            var meanRevealPosition = options.meanRevealPosition;
            var meanRevealPositionDistance = options.meanRevealPositionDistance;
            var meanRevealColour = options.meanRevealColour;
            var meanScreenWidth = options.meanScreenWidth;
            var meanNavPush = options.meanNavPush;
            var meanRevealClass = '.meanmenu-reveal';
            var meanShowChildren = options.meanShowChildren;
            var meanExpandableChildren = options.meanExpandableChildren;
            var meanExpand = options.meanExpand;
            var meanContract = options.meanContract;
            var meanRemoveAttrs = options.meanRemoveAttrs;
            var onePage = options.onePage;
            var meanDisplay = options.meanDisplay;
            var removeElements = options.removeElements;
            var tcuLogo = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 178.1 155.3" height="113" width="129"><path fill-rule="evenodd" clip-rule="evenodd" fill="#4D1979" d="M1 0v114.3l88.3 39.9 87.8-39.9V0z"/><path fill="#FFF" d="M89.3 155.3L0 115V0h2v113.7l87.3 39.4 86.8-39.4V0h2v115l-.6.2z"/><path d="M137.6 77.9c1 0 1.8.8 1.8 1.8s-.8 1.8-1.8 1.8-1.8-.8-1.8-1.8c.1-1 .9-1.8 1.8-1.8zm0 3.9c1.2 0 2.1-.9 2.1-2.1s-.9-2.1-2.1-2.1c-1.2 0-2.1.9-2.1 2.1s.9 2.1 2.1 2.1zm.4-2c.3 0 .6-.2.6-.6 0-.5-.4-.6-.8-.6h-1V81h.3v-1.1h.4l.6 1.1h.4l-.5-1.2zm-.3-.9c.3 0 .5 0 .5.3s-.4.3-.8.3h-.4v-.6h.7zM67.9 33c-17.5 2.3-31.5 8.5-32.1 8.8l-.3.1-.8.4v18.5l2.1-1 .7-.3c.9-.4 3.4-1.6 7-2.9l.3-.1 1-.3V50c.5-.2 1.1-.4 1.6-.6v21c-1.2.5-2.4 1-3.4 1.5l-.5.1-.9.4V85l2.1-1 .7-.3c5.2-2.5 10.5-4.6 15.9-6.4l.3-.1 1-.3V65.3l-1.9.6-.6.2c-.7.2-1.4.4-2.2.6V46.2c.6-.1 1.1-.3 1.6-.4v5.9l1.8-.4.6-.1c2.2-.5 4.4-.9 6.3-1.3l.4-.1 1.2-.2V32.8l-1.4.2h-.5zm28.3-.4l-.4-.4H95c-2-.2-4.3-.3-6.7-.3-3.4 0-6 .2-6.6.3h-.8l-.4.4-8 8.3-.4.4v24l.4.4 7.6 8 .5.5.7-.1h.2c.3 0 3-.2 6.6-.2 2.4 0 4.5.1 6.4.3h.2l.7.1.5-.5 7.9-8.1.4-.4v-8.4l-1.3-.1-8-.9-1.6-.2v4.8l-2.2 2.2c-1 0-1.9-.1-2.9-.1-1.3 0-2.5 0-3.4.1-.5-.5-1.3-1.3-1.7-1.8V45.8c.2-.3.6-.6.9-1 .6-.6 1.3-1.3 1.7-1.8h4.8l2.8 2.8v4.1l1.3.1 8 .8 1.6.2v-9.7l-.4-.4-7.6-8.3zm47.6 10.8l-.3-.1c-4-2-8.7-3.8-14.1-5.4l-.7-.2-1.8-.5v11.5l1 .3c.6.2 1.3.4 1.9.6v18.5c-.6.5-1.5 1.3-2.1 1.8-1.3-.5-3.6-1.3-4.6-1.7-.6-.9-1.8-3.1-2.3-3.9V47c.9.2 1.7.4 1.8.5l1.8.5V36.4l-1.1-.3-.4-.1c-6.6-1.6-12.2-2.5-14.2-2.8l-.6-.1-1.7-.3v11.5l1.3.2.4.1c.3 0 .7.1 1.4.2V67l.2.3 6 9.9.3.5.6.2.2.1c1 .3 6.1 1.8 11.4 4.4l.3.2.9.4.7-.6 10.1-8.7.5-.4V54.1c.4.2.7.4 1.1.6l.7.4 2.1 1V43.8l-.8-.4z" fill="#FDFDFE"/></svg>';

            // detect known mobile/tablet usage
            var isMobile = false;
            if (
                navigator.userAgent.match( /iPhone/i ) ||
                navigator.userAgent.match( /iPod/i ) ||
                navigator.userAgent.match( /iPad/i ) ||
                navigator.userAgent.match( /Android/i ) ||
                navigator.userAgent.match( /Blackberry/i ) ||
                navigator.userAgent.match( /Windows Phone/i )
            ) {
                isMobile = true;
            }

            if ( navigator.userAgent.match( /MSIE 8/i ) || navigator.userAgent.match( /MSIE 7/i ) ) {

                // add scrollbar for IE7 & 8 to stop breaking resize function on small content sites
                jQuery( 'html' ).css( 'overflow-y', 'scroll' );
            }

            var meanRevealPos = '';

            /**
             * Center Menu
             */
            function meanCentered() {
                if ( 'center' === meanRevealPosition ) {
                    var newWidth = window.innerWidth || document.documentElement.clientWidth;
                    var meanCenter = newWidth / 2 - 22 + 'px';
                    meanRevealPos = 'left: ' + meanCenter + '; right: auto;';

                    if ( ! isMobile ) {
                        jQuery( '.meanmenu-reveal' ).css( 'left', meanCenter );
                    } else {
                        jQuery( '.meanmenu-reveal' ).animate( {
                            left: meanCenter
                        } );
                    }
                }
            }

            var menuOn = false;
            var meanMenuExist = false;

            if ( 'right' === meanRevealPosition ) {
                meanRevealPos = 'right: ' + meanRevealPositionDistance + '; left: auto;';
            }
            if ( 'left' === meanRevealPosition ) {
                meanRevealPos = 'left: ' + meanRevealPositionDistance + '; right: auto;';
            }

            // run center function
            meanCentered();

            // set all styles for mean-reveal
            var $navreveal = '';

            /**
             * meanInner function
             */
            function meanInner() {

                // get last class name
                if ( jQuery( $navreveal ).is( '.meanmenu-reveal.meanclose' ) ) {
                    $navreveal.html( meanMenuClose );
                } else {
                    $navreveal.html( meanMenuOpen );
                }
            }

            /**
             *  re-instate original nav (and call this on window.width functions)
             */
            function meanOriginal() {
                jQuery( '.mean-bar,.mean-push' ).remove();
                jQuery( meanContainer ).removeClass( 'mean-container' );
                jQuery( meanMenu ).css( 'display', meanDisplay );
                jQuery( '.tcu-nav-universal' ).css( 'display', meanDisplay );
                menuOn = false;
                meanMenuExist = false;
                jQuery( removeElements ).removeClass( 'mean-remove' );
            }

            /**
             * navigation reveal
             */
            function showMeanMenu() {
                var meanStyles = 'background:' + meanRevealColour + ';color:' + meanRevealColour + ';' + meanRevealPos;
                if ( currentWidth <= meanScreenWidth ) {
                    jQuery( removeElements ).addClass( 'mean-remove' );
                    meanMenuExist = true;

                    // add class to body so we don't need to worry about media queries here, all CSS is wrapped in '.mean-container'
                    jQuery( meanContainer ).addClass( 'mean-container' );
                    jQuery( '.mean-container' ).prepend(
                        '<div class="mean-bar"><a class="tcu-banner__logo" href="http://www.tcu.edu/" rel="nofollow">' + tcuLogo + '<span class="tcu-visuallyhidden">Texas Christian University</span></a><button href="#nav" class="meanmenu-reveal tcu-hamburger--squeeze" style="' +
                            meanStyles +
                            '">Show Navigation</button><nav class="mean-nav"></nav></div>'
                    );

                    // push meanMenu navigation into .mean-nav
                    var meanMenuContents = jQuery( meanMenu ).html();
                    var universalMenu = jQuery( '.tcu-nav-universal__is-mobile' ).html();
                    jQuery( '.mean-nav' ).html( meanMenuContents );
                    jQuery( '.mean-nav .tcu-top-nav' ).append( universalMenu );

                    // remove all classes from EVERYTHING inside meanmenu nav
                    if ( meanRemoveAttrs ) {
                        jQuery( 'nav.mean-nav ul, nav.mean-nav ul *' ).each( function() {

                            // First check if this has mean-remove class
                            if ( jQuery( this ).is( '.mean-remove' ) ) {
                                jQuery( this ).attr( 'class', 'mean-remove' );
                            } else {
                                jQuery( this ).removeAttr( 'class' );
                            }
                            jQuery( this ).removeAttr( 'id' );
                        } );
                    }

                    // push in a holder div (this can be used if removal of nav is causing layout issues)
                    jQuery( meanMenu ).before( '<div class="mean-push" />' );
                    jQuery( '.mean-push' ).css( 'margin-top', meanNavPush );

                    // hide current navigation and reveal mean nav link
                    jQuery( meanMenu ).hide();
                    jQuery( '.tcu-nav-universal__is-mobile' ).hide();
                    jQuery( '.meanmenu-reveal' ).show();

                    // turn 'X' on or off
                    jQuery( meanRevealClass ).html( meanMenuOpen );
                    $navreveal = jQuery( meanRevealClass );

                    // hide mean-nav ul
                    jQuery( '.mean-nav ul' ).hide();

                    // hide sub nav
                    if ( meanShowChildren ) {

                        // allow expandable sub nav(s)
                        if ( meanExpandableChildren ) {
                            jQuery( '.mean-nav ul ul' ).each( function() {
                                if ( jQuery( this ).children().length ) {
                                    var linkText = jQuery(this).prev().text();
                                    jQuery( this, 'li:first' )
                                        .parent()
                                        .append(
                                            '<button class="mean-expand" style="font-size: ' +
                                                meanMenuCloseSize +
                                                '">' +
                                                '<span class="tcu-visuallyhidden">Expand ' +
                                                linkText + 
                                                ' Menu</span>' +
                                                meanExpand +
                                                '</button>'
                                        );
                                }
                            } );
                            jQuery( '.mean-expand' ).on( 'click', function( e ) {
                                e.preventDefault();
                                var linkText = jQuery(this).prev().prev().text();
                                if ( jQuery( this ).hasClass( 'mean-clicked' ) ) {

                                    // use .html method to display unicode character properly
                                  jQuery(this).html(
                                    '<span class="tcu-visuallyhidden">Expand ' +
                                    linkText +
                                    ' Menu</span>' +
                                    meanExpand
                                    );
                                    jQuery( this )
                                        .prev( 'ul' )
                                        .slideUp( 300, function() {} );
                                } else {

                                    // use .html method to display unicode character properly
                                  jQuery(this).html(
                                    '<span class="tcu-visuallyhidden">Contract ' +
                                    linkText +
                                    ' Menu</span>' +
                                    meanContract
                                    );
                                    jQuery( this )
                                        .prev( 'ul' )
                                        .slideDown( 300, function() {} );
                                }
                                jQuery( this ).toggleClass( 'mean-clicked' );
                            } );
                        } else {
                            jQuery( '.mean-nav ul ul' ).show();
                        }
                    } else {
                        jQuery( '.mean-nav ul ul' ).hide();
                    }

                    // add last class to tidy up borders
                    jQuery( '.mean-nav ul li' )
                        .last()
                        .addClass( 'mean-last' );
                    $navreveal.removeClass( 'meanclose' );
                    jQuery( $navreveal ).click( function( e ) {
                        e.preventDefault();
                        if ( false === menuOn ) {
                            $navreveal.css( 'text-align', 'center' );
                            $navreveal.css( 'text-indent', '0' );
                            $navreveal.css( 'font-size', meanMenuCloseSize );
                            jQuery( '.mean-nav ul:first' ).slideDown();
                            menuOn = true;
                        } else {
                            jQuery( '.mean-nav ul:first' ).slideUp();
                            menuOn = false;
                        }

                        // $navreveal.toggleClass("meanclose");
                        $navreveal.toggleClass( 'tcu-is-active' );
                        meanInner();
                        jQuery( removeElements ).addClass( 'mean-remove' );
                    } );

                    // for one page websites, reset all variables...
                    if ( onePage ) {
                        jQuery( '.mean-nav ul > li > a:first-child' ).on( 'click', function() {
                            jQuery( '.mean-nav ul:first' ).slideUp();
                            menuOn = false;
                            jQuery( $navreveal )
                                .toggleClass( 'meanclose' )
                                .html( meanMenuOpen );
                        } );
                    }
                } else {
                    meanOriginal();
                }
            }

            if ( ! isMobile ) {

                // reset menu on resize above meanScreenWidth
                jQuery( window ).resize( function() {
                    currentWidth = window.innerWidth || document.documentElement.clientWidth;
                    if ( currentWidth > meanScreenWidth ) {
                        meanOriginal();
                    } else {
                        meanOriginal();
                    }
                    if ( currentWidth <= meanScreenWidth ) {
                        showMeanMenu();
                        meanCentered();
                    } else {
                        meanOriginal();
                    }
                } );
            }

            jQuery( window ).resize( function() {

                // get browser width
                currentWidth = window.innerWidth || document.documentElement.clientWidth;

                if ( ! isMobile ) {
                    meanOriginal();
                    if ( currentWidth <= meanScreenWidth ) {
                        showMeanMenu();
                        meanCentered();
                    }
                } else {
                    meanCentered();
                    if ( currentWidth <= meanScreenWidth ) {
                        if ( false === meanMenuExist ) {
                            showMeanMenu();
                        }
                    } else {
                        meanOriginal();
                    }
                }
            } );

            // run main menuMenu function on load
            showMeanMenu();
        } );
    };
} )( jQuery );
