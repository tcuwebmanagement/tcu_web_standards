/*eslint wrap-iife: [2, "inside"]*/

// as the page loads, call these scripts
jQuery( document ).ready( function( $ ) {

    // Mobile Menu
    $( '.tcu-main-nav' ).meanmenu( {
        meanRevealPositionDistance: '16px',
        meanMenuOpen:
            '<span class="tcu-hamburger-box"><span class="tcu-hamburger-inner"></span></span><div class="mean-text">Menu</div>',
        meanScreenWidth: '700',
        meanDisplay: '',
        meanExpand:
            '<img src="data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%0D%0A%3C%21--%20Generator%3A%20Adobe%20Illustrator%2019.0.1%2C%20SVG%20Export%20Plug-In%20.%20SVG%20Version%3A%206.00%20Build%200%29%20%20--%3E%0D%0A%3Csvg%20version%3D%221.1%22%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%0D%0A%09%20viewBox%3D%22-77%2010%2020%2011%22%20enable-background%3D%22new%20-77%2010%2020%2011%22%20xml%3Aspace%3D%22preserve%22%3E%0D%0A%3Cpath%20id%3D%22Expand_More%22%20fill%3D%22%23FFFFFF%22%20d%3D%22M-58.7%2C10.3l-8.3%2C8.3l-8.3-8.3c-0.4-0.4-1-0.4-1.4%2C0c-0.4%2C0.4-0.4%2C1%2C0%2C1.4l9%2C9l0%2C0l0%2C0%0D%0A%09c0.4%2C0.4%2C1%2C0.4%2C1.4%2C0l9-9c0.4-0.4%2C0.4-1%2C0-1.4C-57.7%2C9.9-58.3%2C9.9-58.7%2C10.3z%22%2F%3E%0D%0A%3C%2Fsvg%3E%0D%0A" alt="" />',
        meanContract:
            '<img src="data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%0D%0A%3C%21--%20Generator%3A%20Adobe%20Illustrator%2019.0.1%2C%20SVG%20Export%20Plug-In%20.%20SVG%20Version%3A%206.00%20Build%200%29%20%20--%3E%0D%0A%3Csvg%20version%3D%221.1%22%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%0D%0A%09%20viewBox%3D%22-77%2010%2020%2011%22%20enable-background%3D%22new%20-77%2010%2020%2011%22%20xml%3Aspace%3D%22preserve%22%3E%0D%0A%3Cpath%20id%3D%22Expand_Less%22%20fill%3D%22%23FFFFFF%22%20d%3D%22M-66.3%2C10.3c-0.4-0.4-1-0.4-1.4%2C0l-9%2C9c-0.4%2C0.4-0.4%2C1%2C0%2C1.4c0.4%2C0.4%2C1%2C0.4%2C1.4%2C0l8.3-8.3%0D%0A%09l8.3%2C8.3c0.4%2C0.4%2C1%2C0.4%2C1.4%2C0c0.4-0.4%2C0.4-1%2C0-1.4L-66.3%2C10.3z%22%2F%3E%0D%0A%3C%2Fsvg%3E%0D%0A" alt="" />',
        meanNavPush: '16px'
    } );

    // Responsive Tabs
    $( '.tcu-responsive-tabs' ).responsiveTabs( {
        startCollapsed: 'accordion',
        setHash: true,
        animation: 'slide',
        duration: 400,
        rotate: false,
        scrollToAccordion: true,
        scrollToAccordionOnLoad: true,
        classes: {
            stateDefault: 'tcu-tabs-state-default',
            stateActive: 'tcu-tabs-state-active',
            stateDisabled: 'tcu-tabs-state-disabled',
            stateExcluded: 'tcu-tabs-state-excluded',
            container: 'tcu-tabs',
            ul: 'tcu-tabs-nav',
            tab: 'tcu-tabs-tab',
            anchor: 'tcu-tabs-anchor',
            panel: 'tcu-tabs-panel',
            accordionTitle: 'tcu-tabs-accordion-title'
        }
    } );
} ); /* end of as page load scripts */

( function( window, jQuery ) {

    // Targets the main menu
    window.tcuCreateDropDown( {
        target: '.tcu-top-nav',
        minScreenWidth: 701
    } );

    // Targets the universal navigation
    window.tcuCreateDropDown( {
        target: '.tcu-nav-universal',
        hasChildrenSelector: '.menu-item-has-children',
        minScreenWidth: 701
    } );

    // Target Side navigation
    window.tcuCreateDropDown( {
        target: '.tcu-sidebar .widget_nav_menu .menu',
        hasChildrenSelector: '.menu-item-has-children',
        widgetTitle: '.tcu-sidebar__widget-title',
        activeClass: 'tcunavmenu_active',
        expandClass: 'tcu-expand',
        contractClass: 'tcu-contract',
        minScreenWidth: 0,
        useVelocity: true
    } );

    /**
     * Instatiate accordions
     * See tcu.accordions.js for more options
     */
    window.tcuAccordions();

    /**
     * Scroll to top button
     */
    // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset = 300;

    // browser window scroll (in pixels) after which the "back to top" link opacity is reduced
    var offsetOpacity = 1200;

    // duration of the top scrolling animation (in ms)
    var scrollTopDuration = 700;

    // grab the "back to top" link
    var $backToTop = document.querySelector( '.tcu-top' );
    var lastKnownScrollPosition = 0;
    var ticking = false;

    // Bail if $backToTop is null
    if ( ! $backToTop || null === $backToTop ) {
        return;
    }

    window.addEventListener( 'scroll', scrollListener );

    $backToTop.addEventListener( 'click', scrollToTopListener );

    /* --------------------------------- Function Declarations -------------------------------------*/

    /**
     * This function is executed during the scrolling event.
     */
    function scrollListener() {
        lastKnownScrollPosition = window.scrollY;
        if ( ! ticking ) {
            window.requestAnimationFrame( function() {
                swapClass( lastKnownScrollPosition, $backToTop );
                ticking = false;
            } );
        }
        ticking = true;
    }

    /**
     * This function is executed on our click event listener for the
     * scroll to top button
     */
    function scrollToTopListener() {
        var $body = document.body;

        // Give it a tabindex=-1 to remove the focus for screen readers.
        $backToTop.setAttribute( 'tabindex', -1 );

        // Check if jQuery is added
        if ( window.jQuery ) {
            jQuery.Velocity( $body, 'scroll', { duration: scrollTopDuration, easing: 'ease', opacity: 1 } );
        } else {
            window.Velocity( $body, 'scroll', { duration: scrollTopDuration, easing: 'ease', opacity: 1 } );
        }
    }

    /**
     * Swap classes during scroll event.
     *
     * @param {Int}  scrollPos   The position within the window
     * @param {HTML} element     The node element
     */
    function swapClass( scrollPos, element ) {
        if ( scrollPos > offset ) {
            element.classList.add( 'tcu-is-visible' );
        } else {
            element.classList.remove( 'tcu-is-visible' );
            element.classList.remove( 'tcu-fade-out' );
            element.setAttribute( 'tabindex', 0 );
        }
        if ( scrollPos > offsetOpacity ) {
            element.classList.add( 'tcu-fade-out' );
        }
    }
} )( window, jQuery );
