/*eslint wrap-iife: [2, "inside"]*/

/**
 * This file adds a expand/contract button to each sub-menu.
 * It then adds a tcu-visible and tcu-nonvisible class to hide and show the sub-menu.
 * It also adds and aria-hidden attribute for accessibility. Sub-menu works with keyboard.
 *
 * Usage
 *
 *  tcuCreateDropDown({
 *     // The selector for the unordered list (UL) tag (ONLY ONE NODE ELEMENT)
 *     menuTarget: '.tcu-top-nav',
 *
 *     // The selector for title of the widget in sidebar
 *     // (We use this to add an arrow to the heading tag)
 *     widgetTitle: false,
 *
 *     // The selector for the LI with children
 *     hasChildrenSelector: '.menu-item-has-children',
 *
 *     // The selector for the UL with children
 *     subMenuSelector: '.sub-menu',
 *
 *     // The class name to add to UL menu when active
 *     activeClass: 'tcu-menu-active',
 *
 *     // Expand element/text
 *     expandText: '<span class="tcu-visuallyhidden" aria-hidden="true">Expand Menu</span>',
 *
 *     // Contract element/text
 *     contractText: '<span class="tcu-visuallyhidden" aria-hidden="true">Contract Menu</span>',
 *
 *     // The class name to use on the expand button
 *     expandClass: 'tcu-expand--dropdown',
 *
 *     // The class name to use on the contract button
 *     contractClass: 'tcu-contract--dropdown',
 *
 *     // The class name to use in the UL with children to hide
 *     visibleClass: 'tcu-visible',
 *
 *     // The class name to use in the UL with children to show
 *     nonVisibleClass: 'tcu-nonvisible',
 *
 *     // The minimun size screen to activate this script
 *     minScreenWidth: 480,
 *
 *     // Use velocity.js for slideUp/slideDown animation
 *     useVelocity: false,
 *
 * });
 *
 * @author    Mayra Perales <m.j.perales@tcu.edu>
 *
 * @summary   Expand/Contract sub-menus on click event.
 *
 * @since     4.4.11
 */

( function( window, jQuery ) {
    'use strict';

    /**
     * Main function
     * Hides/Expand sub-menus
     *
     * @public
     * @param {Object} options List of options to modify the sub-menus
     */
    function tcuCreateDropDown( options ) {

        // Default options
        var defaults = {
            target: '.tcu-top-nav',
            widgetTitle: false,
            hasChildrenSelector: '.menu-item-has-children',
            subMenuSelector: '.sub-menu',
            activeClass: 'tcu-menu-active',
            expandText:
                '<?xml version="1.0" encoding="utf-8"?><svg height="15" width="15" version="1.1" focusable="false" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="-77 10 20 11" enable-background="new -77 10 20 11" xml:space="preserve"><path fill="#" d="M-58.7,10.3l-8.3,8.3l-8.3-8.3c-0.4-0.4-1-0.4-1.4,0c-0.4,0.4-0.4,1,0,1.4l9,9l0,0l0,0c0.4,0.4,1,0.4,1.4,0l9-9c0.4-0.4,0.4-1,0-1.4C-57.7,9.9-58.3,9.9-58.7,10.3z"/></svg><span class="tcu-visuallyhidden">Expand <span class="button-title"></span> Menu</span>',
            contractText:
                '<?xml version="1.0" encoding="utf-8"?><svg height="15" width="15" version="1.1" focusable="false" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="-77 10 20 11" enable-background="new -77 10 20 11" xml:space="preserve"><path d="M-66.3,10.3c-0.4-0.4-1-0.4-1.4,0l-9,9c-0.4,0.4-0.4,1,0,1.4c0.4,0.4,1,0.4,1.4,0l8.3-8.3l8.3,8.3c0.4,0.4,1,0.4,1.4,0c0.4-0.4,0.4-1,0-1.4L-66.3,10.3z"/></svg><span class="tcu-visuallyhidden">Contract <span class="button-title"></span> Menu</span>',
            expandClass: 'tcu-expand--dropdown',
            contractClass: 'tcu-contract--dropdown',
            visibleClass: 'tcu-visible',
            nonVisibleClass: 'tcu-nonvisible',
            minScreenWidth: 480,
            useVelocity: false
        };

        // Combine defaults with user input
        options = extend( defaults, options );

        // Select our menu
        var menu = document.querySelectorAll( options.target );
        var menuLength = menu.length;

        // Bail out if there's no menu
        if ( null === menu || ! menu ) {
            return;
        }

        // We assign the options keys for easier use
        var widgetTitle = options.widgetTitle;
        var hasChildrenSelector = options.hasChildrenSelector;
        var subMenuSelector = options.subMenuSelector;
        var activeClass = options.activeClass;
        var expandText = options.expandText;
        var contractText = options.contractText;
        var minScreenWidth = options.minScreenWidth;
        var expandClass = options.expandClass;
        var contractClass = options.contractClass;
        var visibleClass = options.visibleClass;
        var nonVisibleClass = options.nonVisibleClass;
        var useVelocity = options.useVelocity;

        var currentWidth = window.innerWidth || document.documentElement.clientWidth;
        var menuActive = false;
        var resizeTimeout;
        var isMobile = false;

        /**
         * Let's add all elements with hasChildrenSelector into an array
         */
        var hasChildrenClass = flattenArray( hasChildrenSelector );

        // Detect known mobile/tablet
        if (
            navigator.userAgent.match( /iPhone/i ) ||
            navigator.userAgent.match( /iPod/i ) ||
            navigator.userAgent.match( /iPad/i ) ||
            navigator.userAgent.match( /Android/i ) ||
            navigator.userAgent.match( /Blackberry/i ) ||
            navigator.userAgent.match( /Windows Phone/i )
        ) {
            isMobile = true;
        }

        /**
         * Only instatiate subMenus function if currentWidth is greater than minScreenWidth.
         */
        if ( currentWidth >= minScreenWidth ) {
            init();
        }

        /**
         * Our event listeners
         */
        window.addEventListener( 'resize', resizeThrottler, false );
        window.addEventListener( 'keyup', closeAllOpenSubMenus );
        document.addEventListener( 'click', closeAllOpenSubMenus );

        /**
         * If we include a widget title class name then we append
         * a contract/expand button so we can add an event on click
         */
        if ( widgetTitle ) {
            var menuTitle = [];

            for ( var x = 0; x < menuLength; x++ ) {
                menuTitle.push( menu[x].parentElement.parentElement.querySelector( widgetTitle ) );
            }

            var maxWindowWidth = 700;

            // Bail if we can't find the widget title
            if ( ! menuTitle || null === menuTitle ) {
                return;
            }

            if ( isMobile || currentWidth < maxWindowWidth ) {

                // Append button into menuTitle
                for ( var x = 0; x < menuTitle.length; x++ ) {
                    menuTitle[x].addEventListener( 'click', onClickTitleListener );

                    // Create our button with Safari 9 support
                    var expandButton = document.createElement( 'button' );
                    expandButton.className = expandClass;
                    expandButton.innerHTML = expandText;
                    expandButton.setAttribute( 'aria-haspopup', 'true' );
                    expandButton.setAttribute( 'aria-expanded', 'false' );
                    menuTitle[x].appendChild( expandButton );

                    menu[x].style.display = 'none';
                }
            } else {

                // Append button into menuTitle
                for ( var x = 0; x < menuTitle.length; x++ ) {

                    // Append button with Safari 9 support
                    var contractButton = document.createElement( 'button' );
                    contractButton.className = contractClass;
                    contractButton.innerHTML = contractText;
                    contractButton.setAttribute( 'aria-haspopup', 'true' );
                    contractButton.setAttribute( 'aria-expanded', 'true' );
                    menuTitle[x].appendChild( contractButton );

                    menuTitle[x].addEventListener( 'click', onClickTitleListener );
                }
            }
        }

        // /* -------------------------  Function Declarations ------------------------------- */
        /**
         * Main Function
         *
         * We add an active class to the target menu. Then we find all the menuSubMenuSelector
         * and add an click event to each item. We also append an expandButton to each
         * hasChildrenClass LI. Then we add the menuNonVisibleClass to each item so we can hide it.
         * We also add an aria-hidden and aria-haspopup attribute for accessibility.
         *
         * At the end we update the menuActive variable to make sure we only run
         * this function once during window resize.
         *
         * @private
         */
        function init() {
            if ( true === menuActive ) {
                return;
            }

            // Add tcu-menu-active to menu element
            for ( var x = 0; x < menuLength; x++ ) {
                menu[x].classList.add( activeClass );
            }

            // Bail if we don't have any .menu-item-has-children
            if ( ! hasChildrenClass || null === hasChildrenClass ) {
                return;
            }

            /**
             * Loop through all hasChildrenClass node items and append the expand button
             * Add appropriate aria-labels
             * Also add tcu-nonvisible to the sub-menu UL
             */
            for ( x = 0; x < hasChildrenClass.length; x++ ) {

                // Select the sub-menu UL
                var subMenu = hasChildrenClass[x].querySelector( subMenuSelector );
                var subMenuTitle = hasChildrenClass[x].querySelector( 'a' ).innerText;

                // If we can't find sub-menu then let's make it equal to false
                if ( ! subMenu || null === subMenu ) {
                    subMenu = false;
                }

                // Only if sub-menu exists
                if ( subMenu ) {

                    // Find all anchors in each LI
                    var links = subMenu.querySelectorAll( 'a' );

                    // Add role=menuitem to all LI links.
                    for ( let m = 0; m < links.length; m++ ) {
                        links[m].setAttribute( 'role', 'menuitem' );
                    }

                    /* Add button after the a link -- This will ensure keyboard users can easily nav through menu
                     * Add Safari 9 support
                     */
                    var contractButton = document.createElement( 'button' );
                    contractButton.className = expandClass;
                    contractButton.innerHTML = expandText;
                    contractButton.setAttribute( 'aria-haspopup', 'true' );
                    contractButton.setAttribute( 'aria-expanded', 'false' );

                    // Insert button into the DOM
                    hasChildrenClass[x].insertBefore( contractButton, hasChildrenClass[x].firstElementChild.nextSibling );
                    hasChildrenClass[x].querySelector( '.button-title' ).innerText = subMenuTitle;

                    // Add tcu-nonvisible class to each element
                    subMenu.classList.add( nonVisibleClass );

                    // Hide and label each sub hasChildrenClass[x]
                    subMenu.setAttribute( 'aria-hidden', 'true' );
                    subMenu.setAttribute( 'role', 'menu' );

                    // Add an event listener to buttons
                    hasChildrenClass[x].addEventListener( 'click', onClickListener, false ); // end of event listener

                    // Update variable to true
                    menuActive = true;
                }
            }

            /**
             * Let's make sure we open the sub-menu on the current page.
             *
             * For a friendlier user experience.
             */
            currentPageOpenMenu();
        }

        /**
         * Sets the button title text based on menu target element
         *
         * @public
         * @param {HTML} parent The parent Node of the element title
         * @param {HTML} el     The target node to set the inner text
         * @param {string} text The text value to set in the target node
         */
        function setElementTitle( parent, el, text ) {

            // Set the button title text based on menu target element
            if ( 'tcu-sidebar__widget-title' === parent.className ) {
                el.querySelector( '.button-title' ).innerText = '';
            } else {
                el.querySelector( '.button-title' ).innerText = text;
            }
        }

        /**
         * Shows the sub-menu by replacing the menuExpandClass with menuContractClass.
         * It changes the menuExpandText with menuContractText. It also replaces the
         * targeted sub-menu class with menuVisibleClass and changes the aria-label
         * to false.
         *
         * @public
         * @param {HTML} target          The target element of the click event
         * @param {HTML} currentSubMenu  The parent sub-menu of target element
         */
        function openMenu( target, currentSubMenu ) {
            var subMenuTitle;

            // Set value for subMenuTitle
            if ( target.parentNode.classList.contains( 'tcu-sidebar__widget-title' )  ) {
                subMenuTitle = target.parentElement.querySelector( 'button' ).innerText;
            }  else {
                subMenuTitle = target.parentElement.querySelector( 'a' ).innerText;
            }

            // Replace class of target button
            target.classList.remove( expandClass );
            target.classList.add( contractClass );

            // Change the text to contract menu
            target.innerHTML = contractText;

            // Set the sidebar widget title text
            setElementTitle( target.parentNode, target, subMenuTitle );

            // set attribute
            target.setAttribute( 'aria-expanded', 'true' );

            // Check if user selected to use velocity.js for animation
            if ( useVelocity ) {

                // Replace class of current sub-menu
                currentSubMenu.classList.remove( nonVisibleClass );
                currentSubMenu.classList.add( visibleClass );

                // Check if jQuery is added
                if ( window.jQuery ) {
                    jQuery.Velocity( currentSubMenu, 'slideDown', 300 );
                } else {
                    window.Velocity( currentSubMenu, 'slideDown', 300 );
                }
            } else {

                // Replace class of target sub-menu
                currentSubMenu.classList.remove( nonVisibleClass );
                currentSubMenu.classList.add( visibleClass );
            }

            // set attribute
            currentSubMenu.setAttribute( 'aria-hidden', 'false' );
        }

        /**
         * Hides the sub-menu by replacing the menuContactClass with menuExpandClass.
         * It changes the menuContractText with menuExpandText. It also replaces the
         * targeted sub-menu class with menuNonVisibleClass and changes the aria-label
         * to true.
         *
         * @param {HTML} target          The target element of the click event
         * @param {HTML} currentSubMenu  The parent sub-menu of target element
         */
        function closeMenu( target, currentSubMenu ) {
            var subMenuTitle;

            // Set value for subMenuTitle
            if ( target.parentNode.classList.contains( 'tcu-sidebar__widget-title' )  ) {
                subMenuTitle = target.parentElement.querySelector( 'button' ).innerText;
            }  else {
                subMenuTitle = target.parentElement.querySelector( 'a' ).innerText;
            }

            // Replace class of target button
            target.classList.remove( contractClass );
            target.classList.add( expandClass );

            // Change the text to expand menu
            target.innerHTML = expandText;

            target.querySelector( '.button-title' ).innerText = subMenuTitle;

             // Set the sidebar widget title text
             setElementTitle( target.parentNode, target, subMenuTitle );

            // set attribute
            target.removeAttribute( 'aria-expanded' );

            // Check if user selected to use velocity.js for animation
            if ( useVelocity ) {

                // Replace class of target sub-menu
                currentSubMenu.classList.remove( visibleClass );
                currentSubMenu.classList.add( nonVisibleClass );

                // Check if jQuery is added
                if ( window.jQuery ) {
                    jQuery.Velocity( currentSubMenu, 'slideUp', 300 );
                } else {
                    window.Velocity( currentSubMenu, 'slideUp', 300 );
                }
            } else {

                // Replace class of target sub-menu
                currentSubMenu.classList.remove( visibleClass );
                currentSubMenu.classList.add( nonVisibleClass );
            }

            // set attribute
            currentSubMenu.setAttribute( 'aria-hidden', 'true' );
        }

        /**
         * Keep sub-menu open for the current-page link.
         *
         * Targets only the side navigation.
         *
         * @public
         */
        function currentPageOpenMenu() {
            const listItems = document.querySelectorAll( '.tcunavmenu_active li' );

            // Bail if we don't have any listItems
            if ( ! listItems || null === listItems ) {
                return;
            }

            for ( let x = 0; x < listItems.length; x++ ) {
                if (
                    listItems[x].classList.contains( 'current-menu-parent' ) ||
                    listItems[x].classList.contains( 'current-menu-ancestor' ) ||
                    listItems[x].classList.contains( 'current-menu-parent' ) ||
                    listItems[x].classList.contains( 'current_page_parent' ) ||
                    listItems[x].classList.contains( 'current_page_ancestor' )
                ) {
                    const currentListItem = listItems[x];
                    const currentSubMenu = currentListItem.querySelector( 'ul.sub-menu' );
                    const currentMenuItem = currentSubMenu.querySelector( '.current-menu-item' );
                    const button = currentListItem.querySelector( '.tcu-expand' );

                    if ( currentMenuItem && button ) {

                        // Open Sub-Menu.
                        openMenu( button, currentSubMenu );
                    }
                }
            }
        }

        /**
         * This will instiatiate during the click event
         *
         * @param {Object} event An object that implements the Event interface
         */
        function onClickListener( event ) {

            // Stop events from bubbling up to parent elements
            event.stopPropagation();

            var target = event.target;
            var parentMenu = target.parentNode;
            var currentSubMenu = parentMenu.querySelector( subMenuSelector );
            var allOpenSubMenu = flattenArray( '.' + visibleClass );
            var conctractButton = flattenArray( '.' + contractClass );

            var allOpenSubMenuCount = allOpenSubMenu.length;

            // If target contains expandClass then sub-menu needs to open
            if ( target.classList.contains( expandClass ) && 'BUTTON' === target.nodeName ) {
                openMenu( target, currentSubMenu );

                // Else sub-menu needs to close
            } else if ( 'BUTTON' === target.nodeName ) {
                closeMenu( target, currentSubMenu );
            }

            // Only open one .sub-menu at a time if it's top-level and if menuCloseTopLevel is true
            if ( 0 < allOpenSubMenuCount ) {

                // loop through each
                for ( let x = 0; allOpenSubMenuCount > x; x++ ) {

                    // Check if the open menu is top-level, if so, close it
                    if ( parentMenu.parentNode.classList.contains( activeClass ) ) {
                        closeMenu( conctractButton[x], allOpenSubMenu[x] );
                    }
                }
            }
        }

        /**
         * This will instiatiate during the click event of the widget title
         *
         * @param {Object} event An object that implements the Event interface
         */
        function onClickTitleListener( event ) {
            var currentMenu = event.target.parentElement.parentElement.querySelector( options.target );

            // If target contains menuExpandClass then sub-menu needs to close
            if ( event.target.classList.contains( expandClass ) && 'BUTTON' === event.target.nodeName ) {
                openMenu( event.target, currentMenu );
            } else if ( event.target.classList.contains( contractClass ) && 'BUTTON' === event.target.nodeName ) {
                closeMenu( event.target, currentMenu );
            }
        }

        /**
         * Close all open sub-menus
         *
         * @param {Object} event An object that implements the Event interface
         */
        function closeAllOpenSubMenus( event ) {
            var openSubMenus = flattenArray( '.' + visibleClass );
            var contractButton = flattenArray( '.' + contractClass );

            var body = document.querySelector( 'body' );

            // If event is keyup and it was the ESC key
            if ( 'keyup' === event.type && 27 === event.keyCode ) {

                // Only if we have openSubMenus
                if ( openSubMenus ) {

                    // Loop through all open sub-menus and close them
                    for ( var x = 0; x < openSubMenus.length; x++ ) {
                        closeMenu( contractButton[x], openSubMenus[x] );
                    }
                }

                // if event is click and it's the HTML body
            } else if ( 'click' === event.type && body ) {
                if ( openSubMenus ) {

                    // Loop through all open sub-menus and close them
                    for ( var x = 0; x < openSubMenus.length; x++ ) {
                        closeMenu( contractButton[x], openSubMenus[x] );
                    }
                }
            }

            // If Enter key is pressed
            // Move focus to the first LI anchor
            if ( 13 === event.keyCode ) {
                const parent = event.target.parentNode;

                // Check if target belongs to our menu.
                if ( parent.classList.contains( hasChildrenSelector.slice( 1 ) ) ) {
                    const subMenu = parent.querySelector( subMenuSelector );
                    const firstLink = subMenu.querySelector( 'a' );
                    firstLink.focus();
                }
            }
        }

        /**
         * Merge defaults with user options
         *
         * @private
         * @param   {Object} defaults Default settings
         * @param   {Object} options  User options
         * @returns {Object} Merged values of defaults and options
         */
        function extend( defaults, options ) {
            var extended = {};
            var prop;
            for ( prop in defaults ) {
                if ( Object.prototype.hasOwnProperty.call( defaults, prop ) ) {
                    extended[prop] = defaults[prop];
                }
            }
            for ( prop in options ) {
                if ( Object.prototype.hasOwnProperty.call( options, prop ) ) {
                    extended[prop] = options[prop];
                }
            }
            return extended;
        }

        /**
         * Destroy menu by removing buttons that were appended. We also remove
         * the activeClass from the UL element. Our mobile menu script copies
         * the appended buttons so we need to make sure we remove them too. Updates
         * the menuActive variable to false so our main function can instatiate on window
         * resize.
         *
         * @private
         */
        function destroy() {
            var expandButtons = flattenArray( '.' + expandClass );
            var contractButtons = flattenArray( '.' + contractClass );
            var menuList = document.querySelector( '.tcu-top-nav' );

            if ( menuList ) {
                var eButtons = menuList.querySelectorAll( '.' + expandClass );
                var cButtons = menuList.querySelectorAll( '.' + contractClass );

                // remove tcu-menu-active to menu element
                menuList.classList.remove( activeClass );

                if ( eButtons ) {
                    for ( var counter = 0; counter < eButtons.length; counter++ ) {
                        eButtons[counter].parentElement.removeChild( eButtons[counter] );
                    }
                }

                if ( cButtons ) {
                    for ( counter = 0; counter < cButtons.length; counter++ ) {
                        cButtons[counter].parentElement.removeChild( cButtons[counter] );
                    }
                }
            }

            // remove tcu-menu-active class from menu
            for ( var x = 0; x < menuLength; x++ ) {
                menu[x].classList.remove( activeClass );
            }

            if ( expandButtons ) {
                for ( counter = 0; counter < expandButtons.length; counter++ ) {
                    expandButtons[counter].parentElement.removeChild( expandButtons[counter] );
                }
            }

            if ( contractButtons ) {
                for ( counter = 0; counter < contractButtons.length; counter++ ) {
                    contractButtons[counter].parentElement.removeChild( contractButtons[counter] );
                }
            }

            menuActive = false;
        }

        /**
         * We call this function in the resize event listener.
         * Ignores the resize events as long as the initSubMenu execution is in the queue.
         *
         * @private
         */
        function resizeThrottler() {
            if ( ! resizeTimeout ) {
                resizeTimeout = setTimeout( function() {
                    resizeTimeout = null;
                    initSubMenu();
                }, 66 );
            }
        }

        /**
         * Push elements into an array and concat the multi level array.
         * This function loops through the menu array.
         *
         * @param {String} querySelector String to use within querySelectorAll
         * @private
         * @return {Array} flatArray   One level array for easier access
         */
        function flattenArray( querySelector ) {
            var items = [];

            // loop through the menu array and push found elements into items
            for ( var x = 0; x < menuLength; x++ ) {
                items.push( menu[x].querySelectorAll( querySelector ) );
            }

            // let's flatten our flatArray array
            var flatArray = items.reduce( function( array, node ) {
                for ( var c = 0; c < node.length; c++ ) {
                    array.push( node[c] );
                }
                return array;
            }, [] );

            return flatArray;
        }

        /**
         * Run this function during the resize event.
         * Check the user's current window width and compare it to minScreenWidth.
         *
         * @private
         */
        function initSubMenu() {
            currentWidth = window.innerWidth || document.documentElement.clientWidth;

            if ( currentWidth >= minScreenWidth ) {
                init();
            } else {
                destroy();
            }
        }
    } // end of tcuCreateDropDown()

    // add to global namespace
    window.tcuCreateDropDown = tcuCreateDropDown;
} )( window, jQuery );
