/*eslint wrap-iife: [2, "inside"]*/

/**
 * Adds a dropdown button into the WP Visual Editor that
 * includes our shortcode injections.
 *
 * @author Mayra Perales <m.j.perales@tcu.edu>
 *
 * @summary Adds dropdown button to Visual Editor
 *
 * @since 1.1.0
 */
( function() {
    window.tinymce.PluginManager.add( 'tcu_mce_button', function( editor ) {
        editor.addButton( 'tcu_mce_button', {
            text: 'Button/Styles',
            icon: false,
            type: 'menubutton',
            menu: [
                {
                    text: 'Add Primary Button',
                    onclick: function() {
                        editor.windowManager.open( {
                            title: 'Button: Options',
                            body: [
                                {
                                    type: 'textbox',
                                    name: 'linkAtt',
                                    label: 'URL'
                                },
                                {
                                    type: 'textbox',
                                    name: 'buttonText',
                                    label: 'Link Text'
                                },
                                {
                                    type: 'textbox',
                                    name: 'ariaLabel',
                                    label: 'ARIA label'
                                },
                                {
                                    type: 'textbox',
                                    name: 'idAtt',
                                    label: 'Custom ID'
                                },
                            ],
                            onsubmit: function( e ) {
                                var buttonShortCode = '[button';

                                if ( e.data.ariaLabel ) {
                                    buttonShortCode += ' aria-label="' + e.data.ariaLabel + '"';
                                }

                                if ( e.data.idAtt ) {
                                    buttonShortCode += ' id="' + e.data.idAtt + '"';
                                }

                                buttonShortCode += ' link="' + e.data.linkAtt + '"]' + e.data.buttonText + '[/button]';
                                editor.insertContent( buttonShortCode );
                            }
                        } );
                    }
                },
                {
                    text: 'Add Secondary Button',
                    onclick: function() {
                        editor.windowManager.open( {
                            title: 'Button: Options',
                            body: [
                                {
                                    type: 'textbox',
                                    name: 'linkAtt',
                                    label: 'URL'
                                },
                                {
                                    type: 'textbox',
                                    name: 'buttonText',
                                    label: 'Link Text'
                                },
                                {
                                    type: 'textbox',
                                    name: 'ariaLabel',
                                    label: 'ARIA label'
                                },
                                {
                                    type: 'textbox',
                                    name: 'idAtt',
                                    label: 'Custom ID'
                                },
                            ],
                            onsubmit: function( e ) {
                                var buttonShortCode = '[button_secondary';

                                if ( e.data.ariaLabel ) {
                                    buttonShortCode += ' aria-label="' + e.data.ariaLabel + '"';
                                }

                                if ( e.data.idAtt ) {
                                    buttonShortCode += ' id="' + e.data.idAtt + '"';
                                }

                                buttonShortCode += ' link="' + e.data.linkAtt + '"]' + e.data.buttonText + '[/button_secondary]';
                                editor.insertContent( buttonShortCode );
                            }
                        } );
                    }
                },
                {
                    text: 'Add Quote',
                    onclick: function() {
                        editor.windowManager.open( {
                            title: 'Quote: Options',
                            body: [
                                {
                                    type: 'textbox',
                                    minWidth: 400,
                                    minHeight: 100,
                                    multiline: true,
                                    resizable: true,
                                    name: 'quoteText',
                                    label: 'Text Area'
                                }
                            ],
                            onsubmit: function( e ) {
                                editor.insertContent( '[quote]' + e.data.quoteText + '[/quote]' );
                            }
                        } );
                    }
                },
                {
                    text: 'Add Two Columns',
                    onclick: function() {
                        editor.windowManager.open( {
                            title: 'Two Columns',
                            body: [
                                {
                                    type: 'container',
                                    name: 'Instructions',
                                    layout: 'stack',
                                    html:
                                        '<div><strong>Instructions</strong><p>Paste your content into the textbox. Style the copy and add images/video through the Visual Editor after you click the "ok" button.</p></div>'
                                },
                                {
                                    type: 'textbox',
                                    minWidth: 300,
                                    minHeight: 300,
                                    multiline: true,
                                    layout: 'fit',
                                    resizable: true,
                                    name: 'twoColumnText',
                                    label: 'Content Area'
                                }
                            ],
                            onsubmit: function( e ) {
                                editor.insertContent( '[two_columns]' + e.data.twoColumnText + '[/two_columns]' );
                            }
                        } );
                    }
                },
                {
                    text: 'Add Three Columns',
                    onclick: function() {
                        editor.windowManager.open( {
                            title: 'Three Columns',
                            body: [
                                {
                                    type: 'container',
                                    name: 'Instructions',
                                    layout: 'stack',
                                    html:
                                        '<div><strong>Instructions</strong><p>Paste your content into the textbox. Style the copy and add images/video through the Visual Editor after you click the "ok" button.</p></div>'
                                },
                                {
                                    type: 'textbox',
                                    minWidth: 300,
                                    minHeight: 300,
                                    multiline: true,
                                    layout: 'fit',
                                    name: 'threeColumnText',
                                    label: 'Content Area'
                                }
                            ],
                            onsubmit: function( e ) {
                                editor.insertContent( '[three_columns]' + e.data.threeColumnText + '[/three_columns]' );
                            }
                        } );
                    }
                }
            ]
        } );
    } );
} )();
