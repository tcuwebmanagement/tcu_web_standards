/*eslint wrap-iife: [2, "inside"]*/

/**
 * This file adds the accordion functionality. It adds an inactiveClass to all headings.
 * Then it opens the first accordion element by adding an activeClass to it and adding the openClass
 * to it's sibling. We also make sure that we add correct aria- labels for accessiblity support. We add
 * an event listener to all headings so we can open/close the accordion content.
 *
 * @author Mayra Perales <m.j.perales@tcu.edu>
 *
 * @summary Open/Close accordions
 *
 * @since 4.5.12
 */
( function( window ) {
    'use strict';

    /**
     *  Main function
     * Sets default options and instatiates accordions.
     *
     * @param {Object} options  List of options to modify accordions
     */
    function tcuAccordions( options ) {

        // default options
        var defaults = {
            target: '.tcu-accordion-container',
            accordionHeader: '.tcu-accordion-header',
            accordionContent: '.tcu-accordion-content',
            inactiveClass: 'tcu-inactive-header',
            activeClass: 'tcu-active-header',
            openClass: 'tcu-open-content'
        };

        // Combine defaults with user input
        options = extend( defaults, options );

        // Select accordion element
        const accordion = document.querySelectorAll( options.target );
        const accordionLength = accordion.length;

        // Bail if there's no accordion element
        if ( null === accordion || ! accordion ) {
            return;
        }

        // Our variables for easier use
        const accordionHeader = options.accordionHeader;
        const accordionContent = options.accordionContent;
        const inactiveClass = options.inactiveClass;
        const activeClass = options.activeClass;
        const openClass = options.openClass;
        const firstHeading = flattenArray( accordionHeader + ':first-of-type' );
        const firstContent = flattenArray( accordionContent + ':first-of-type' );
        const contentElements = flattenArray( accordionContent );
        const allHeadings = flattenArray( accordionHeader );

        // instatiate accordions
        init();

        /********************* Function Declarations ************************/

        /**
         * Init function
         *
         * @private
         */
        function init() {

            /**
             * Loop through accordion content and add aria-labels
             */
            for ( let x = 0; x < contentElements.length; x++ ) {

                // Add aria labels
                contentElements[x].setAttribute( 'aria-hidden', 'true' );
                contentElements[x].setAttribute( 'aria-label', 'Accordion Content' );
            }

            /**
             * Loop through all headings and add an event click.
             */
            for ( let x = 0; x < allHeadings.length; x++ ) {

                // add inactive class to headings
                allHeadings[x].classList.add( inactiveClass );

                // Add downArrow image tag.
                const downArrow = document.createElement( 'img' );
                downArrow.height = 20;
                downArrow.width = 20;
                downArrow.alt = 'Open Accordion';
                downArrow.classList.add( 'tcu-down-arrow' );
                downArrow.src =
                    'data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%0D%0A%3C%21--%20Generator%3A%20Adobe%20Illustrator%2019.0.1%2C%20SVG%20Export%20Plug-In%20.%20SVG%20Version%3A%206.00%20Build%200%29%20%20--%3E%0D%0A%3Csvg%20version%3D%221.1%22%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%0D%0A%09%20viewBox%3D%22-77%2010%2020%2011%22%20enable-background%3D%22new%20-77%2010%2020%2011%22%20xml%3Aspace%3D%22preserve%22%3E%0D%0A%3Cpath%20id%3D%22Expand_More%22%20fill%3D%22%23FFFFFF%22%20d%3D%22M-58.7%2C10.3l-8.3%2C8.3l-8.3-8.3c-0.4-0.4-1-0.4-1.4%2C0c-0.4%2C0.4-0.4%2C1%2C0%2C1.4l9%2C9l0%2C0l0%2C0%0D%0A%09c0.4%2C0.4%2C1%2C0.4%2C1.4%2C0l9-9c0.4-0.4%2C0.4-1%2C0-1.4C-57.7%2C9.9-58.3%2C9.9-58.7%2C10.3z%22%2F%3E%0D%0A%3C%2Fsvg%3E%0D%0A';

                // Add img element to first heading.
                allHeadings[x].appendChild( downArrow );

                // Let screen readers know collapsable content below is in the expanded or in the collapsed state
                allHeadings[x].setAttribute( 'aria-expanded', 'false' );

                // Add click event to all headings
                allHeadings[x].addEventListener( 'click', onClickListener );
            }

            // Open first accordion and add aria-expanded true to first headings
            for ( let x = 0; x < firstHeading.length; x++ ) {
                openAccordion( firstHeading[x], firstContent[x] );
                firstHeading[x].setAttribute( 'aria-expanded', 'true' );
            }
        }

        /**
         * Click event listener on all accordion headings
         *
         * @param {Node} event Click event target
         */
        function onClickListener( event ) {
            const target = event.target;
            const sibling = target.nextElementSibling;
            const openElements = target.parentNode.querySelectorAll( '.' + activeClass );
            const openElementsCount = openElements.length;

            if ( target.classList.contains( inactiveClass ) ) {
                openAccordion( target, sibling );
            } else if ( target.classList.contains( activeClass ) ) {
                closeAccordion( target, sibling );
            }

            // only open one accordion at a time
            if ( 0 < openElementsCount ) {
                for ( let x = 0; x < openElementsCount; x++ ) {
                    closeAccordion( openElements[x], openElements[x].nextElementSibling );
                }
            }
        }

        /**
         * Displays the accordion content by replacing the inactiveClass.
         * It adds the openClass to sibling. This also changes the
         * aria-hidden to false.
         *
         * @param {Node} target  Click event target (should be the heading)
         * @param {Node} sibling Event's sibling (should be the content)
         */
        function openAccordion( target, sibling ) {
            const currentImage = target.querySelector( 'img' );
            const upArrow = document.createElement( 'img' );
            upArrow.height = 20;
            upArrow.width = 20;
            upArrow.alt = 'Close Accordion';
            upArrow.src =
                'data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%0D%0A%3C%21--%20Generator%3A%20Adobe%20Illustrator%2019.0.1%2C%20SVG%20Export%20Plug-In%20.%20SVG%20Version%3A%206.00%20Build%200%29%20%20--%3E%0D%0A%3Csvg%20version%3D%221.1%22%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%0D%0A%09%20viewBox%3D%22-77%2010%2020%2011%22%20enable-background%3D%22new%20-77%2010%2020%2011%22%20xml%3Aspace%3D%22preserve%22%3E%0D%0A%3Cpath%20id%3D%22Expand_Less%22%20fill%3D%22%23FFFFFF%22%20d%3D%22M-66.3%2C10.3c-0.4-0.4-1-0.4-1.4%2C0l-9%2C9c-0.4%2C0.4-0.4%2C1%2C0%2C1.4c0.4%2C0.4%2C1%2C0.4%2C1.4%2C0l8.3-8.3%0D%0A%09l8.3%2C8.3c0.4%2C0.4%2C1%2C0.4%2C1.4%2C0c0.4-0.4%2C0.4-1%2C0-1.4L-66.3%2C10.3z%22%2F%3E%0D%0A%3C%2Fsvg%3E%0D%0A';

            // replace class of target
            target.classList.remove( inactiveClass );
            target.classList.add( activeClass );

            // Remove downArrow and replace with upArrow
            target.removeChild( currentImage );
            target.appendChild( upArrow );

            // Change the heading aria-expanded to true.
            target.setAttribute( 'aria-expanded', 'true' );

            // Check if jQuery is added
            if ( window.jQuery ) {
                jQuery.Velocity( sibling, 'slideDown', 300 );
            } else {
                window.Velocity( sibling, 'slideDown', 300 );
            }

            // replace class for sibling
            sibling.classList.add( openClass );

            // change aria labels for sibling
            sibling.setAttribute( 'aria-hidden', 'false' );
        }

        /**
         * Hides the accordion content by replacing the activeClass.
         * It removes the openClass from sibling. This also changes the
         * aria-hidden to true.
         *
         * @param {Node} target  Click event target (should be the heading)
         * @param {Node} sibling Event's sibling (should be the content)
         */
        function closeAccordion( target, sibling ) {
            const currentImage = target.querySelector( 'img' );
            const downArrow = document.createElement( 'img' );
            downArrow.height = 20;
            downArrow.width = 20;
            downArrow.alt = 'Open Accordion';
            downArrow.src =
                'data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22utf-8%22%3F%3E%0D%0A%3C%21--%20Generator%3A%20Adobe%20Illustrator%2019.0.1%2C%20SVG%20Export%20Plug-In%20.%20SVG%20Version%3A%206.00%20Build%200%29%20%20--%3E%0D%0A%3Csvg%20version%3D%221.1%22%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%0D%0A%09%20viewBox%3D%22-77%2010%2020%2011%22%20enable-background%3D%22new%20-77%2010%2020%2011%22%20xml%3Aspace%3D%22preserve%22%3E%0D%0A%3Cpath%20id%3D%22Expand_More%22%20fill%3D%22%23FFFFFF%22%20d%3D%22M-58.7%2C10.3l-8.3%2C8.3l-8.3-8.3c-0.4-0.4-1-0.4-1.4%2C0c-0.4%2C0.4-0.4%2C1%2C0%2C1.4l9%2C9l0%2C0l0%2C0%0D%0A%09c0.4%2C0.4%2C1%2C0.4%2C1.4%2C0l9-9c0.4-0.4%2C0.4-1%2C0-1.4C-57.7%2C9.9-58.3%2C9.9-58.7%2C10.3z%22%2F%3E%0D%0A%3C%2Fsvg%3E%0D%0A';

            // replace class of target
            target.classList.remove( activeClass );
            target.classList.add( inactiveClass );

            // Replace upArrow with downArrow
            target.removeChild( currentImage );
            target.appendChild( downArrow );

            // Change the heading aria-expanded to false.
            target.setAttribute( 'aria-expanded', 'false' );

            // Check if jQuery is added
            if ( window.jQuery ) {
                jQuery.Velocity( sibling, 'slideUp', 300 );
            } else {
                window.Velocity( sibling, 'slideUp', 300 );
            }

            // replace class for sibling
            sibling.classList.remove( openClass );

            // change aria labels for sibling
            sibling.setAttribute( 'aria-hidden', 'true' );
        }

        /**
         * Merge defaults with user options
         *
         * @private
         * @param   {Object} defaults Default settings
         * @param   {Object} options  User options
         * @returns {Object} Merged values of defaults and options
         */
        function extend( defaults, options ) {
            const extended = {};
            for ( const prop in defaults ) {
                if ( Object.prototype.hasOwnProperty.call( defaults, prop ) ) {
                    extended[prop] = defaults[prop];
                }
            }
            for ( const prop in options ) {
                if ( Object.prototype.hasOwnProperty.call( options, prop ) ) {
                    extended[prop] = options[prop];
                }
            }
            return extended;
        }

        /**
         * Push elements into an array and concat the multi level array.
         * This function loops through the accordion array.
         *
         * @param {String} querySelector String to use within querySelectorAll
         * @private
         * @return {Array} flatArray   One level array for easier access
         */
        function flattenArray( querySelector ) {
            const items = [];

            // loop through the menu array and push found elements into items
            for ( let x = 0; x < accordionLength; x++ ) {
                items.push( accordion[x].querySelectorAll( querySelector ) );
            }

            // let's flatten our flatArray array
            const flatArray = items.reduce( function( array, node ) {
                for ( let c = 0; c < node.length; c++ ) {
                    array.push( node[c] );
                }
                return array;
            }, [] );

            return flatArray;
        }
    } // end of tcuAccordions();

    // add to global namespace
    window.tcuAccordions = tcuAccordions;
} )( window );
