<?php
/**
 * Google Analytics Settings
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 1.0.0
 *
 * ! @deprecated 4.7.0 No longer used by internal code and not recommended.
 * @see filename tcu-google-analytics-settings.php
 */

// Add settings submenu.
add_action( 'admin_menu', 'tcu_admin_menu' );

/**
 *  Create the sub-menu
 */
function tcu_admin_menu() {
	add_options_page( 'Google Analytics', 'Google Analytics', 'manage_options', 'tcu_admin_settings_page', 'tcu_admin_settings_page' );
}

/**
 * Build the settings page
 */
function tcu_admin_settings_page() {

	// Load the plugin options array.
	$admin_options_arr = get_option( 'tcu_admin_options' );

	// Set the option array values to variables.
	$g_script = ( ! empty( $admin_options_arr['g_script'] )) ? $admin_options_arr['g_script'] : '' ; ?>

	<div class="wrap">
		<h2><?php esc_html_e( 'TCU Google Analytics Settings' ); ?></h2>
		<table class="form-table">
			<form method="post" action="options.php">
				<?php settings_fields( 'tcu-admin-settings-group' ); ?>
				<tr valign="top">
					<th scope="row"><?php esc_html_e( 'Google Analytics Code' ); ?></th>
					<td><textarea id="tcu_admin_options[g_script]" rows="10" class="widefat large-text code" name="tcu_admin_options[g_script]"><?php echo esc_textarea( $admin_options_arr['g_script'] ); ?></textarea><br />
					<p class="description"><?php esc_html_e( 'Use the input field above to paste the full code that comes from your google analytics account pertaining to this website.' ); ?></p></td>
				</tr>
			</table>
			<p class="submit"><input type="submit" class="button-primary" value="<?php esc_attr_e( 'Save Changes', 'tcu-admin' ); ?>" /></p>
		</form>
	</div>
<?php
}

// Action hook to register the option settings.
add_action( 'admin_init', 'tcu_admin_register_settings' );

/**
 * Register the array of settings.
 */
function tcu_admin_register_settings() {
	register_setting( 'tcu-admin-settings-group', 'tcu_admin_options', 'tcu_admin_sanitize' );
}

/**
 * Lets santize the input and only allow script tags
 *
 * @param array $options  List of allowed HTML tags.
 * @return array $options
 */
function tcu_admin_sanitize( $options ) {

	// Array of allowed html tags.
	$html = array(
		'script' => array(),
	);

	$options['g_script'] = ( ! empty( $options['g_script'] ) ) ? wp_kses( $options['g_script'], $html ) : '';

	return $options;
}

/**
 * Print the script into the footer.
 */
function tcu_google_analytics_script() {

	// Let's grab the Google Analytics script from database.
	$script = get_option( 'tcu_admin_options' );

	echo wp_kses( $script['g_script'], array( 'script' => array() ) );
}

add_action( 'wp_footer', 'tcu_google_analytics_script' );
