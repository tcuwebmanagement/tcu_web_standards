<?php
/**
 * Breadcrumbs Settings
 *
 * College/Division Name
 * College/Division URL
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 1.0.0
 *
 * ! @deprecated 4.7.0 No longer used by internal code and not recommended.
 * @see filename tcu-breadcrumbs-settings.php
 */

/**
 * Callback function to add Breadcrumb Section
 */
function tcu_breadcrumbs_settings_init() {

	// Add Breadcrumbs Settings Section to the reading page.
	add_settings_section(
		'tcu_breadcrumbs_settings_section',
		'TCU Breadcrumbs Settings',
		'tcu_breadcrumbs_settings_callback',
		'reading'
	);

	// Add Breadcrumbs Name field.
	add_settings_field(
		'tcu_breadcrumbs_name',
		'College/Division Name',
		'tcu_breadcrumbs_name_callback',
		'reading',
		'tcu_breadcrumbs_settings_section'
	);

	// Add Breadcrumbs URL field.
	add_settings_field(
		'tcu_breadcrumbs_url',
		'College/Division URL',
		'tcu_breadcrumbs_url_callback',
		'reading',
		'tcu_breadcrumbs_settings_section'
	);

	/**
	 * Register our setting so that $_POST handling is done for us and
	 * our callback function just has to echo the <input>
	 */
	register_setting( 'reading', 'tcu_breadcrumbs_name' );
	register_setting( 'reading', 'tcu_breadcrumbs_url' );
}

add_action( 'admin_init', 'tcu_breadcrumbs_settings_init' );

/**
 * Breadcrumbs section callback function
 */
function tcu_breadcrumbs_settings_callback() {
	echo '<p>Leave blank if this website is a root division or college.</p>';
}

/**
 * College/Division text input
 */
function tcu_breadcrumbs_name_callback() {
	$bc_name = ( ! empty( get_option( 'tcu_breadcrumbs_name' ) ) ? get_option( 'tcu_breadcrumbs_name' ) : '' );
	echo '<input class="regular-text" name="tcu_breadcrumbs_name" id="tcu_breadcrumbs_name" type="text" value="' . esc_attr( $bc_name ) . '">';
}

/**
 * College/Division url input
 */
function tcu_breadcrumbs_url_callback() {
	$bc_url = ( ! empty( get_option( 'tcu_breadcrumbs_url' ) ) ? get_option( 'tcu_breadcrumbs_url' ) : '' );
	echo '<input class="regular-text" name="tcu_breadcrumbs_url" id="tcu_breadcrumbs_url" type="url" value="' . esc_attr( $bc_url ) . '">';
}
