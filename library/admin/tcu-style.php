<?php
/**
 * Customizes the UI , login page and other admin pages.
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 1.0.0
 */

/**
 * Disable default dashboard widgets
 */
function tcu_web_standards_disable_default_dashboard_widgets() {
	global $wp_meta_boxes;

	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments'] ); // Comments Widget.
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links'] );  // Incoming Links Widget.
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'] );         // Plugins Widget.

	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press'] );       // Quick Press Widget.
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts'] );     // Recent Drafts Widget.
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'] );
	// Remove plugin dashboard boxes.
	unset( $wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget'] );           // Yoast's SEO Plugin .
	unset( $wp_meta_boxes['dashboard']['normal']['core']['rg_forms_dashboard'] );        // Gravity Forms Plugin .
	unset( $wp_meta_boxes['dashboard']['normal']['core']['bbp-dashboard-right-now'] );   // bbPress Plugin .
}

// Removing the dashboard widgets.
add_action( 'wp_dashboard_setup', 'tcu_web_standards_disable_default_dashboard_widgets' );

/**
 * Calling our own login css
 */
function tcu_web_standards_login_css() {
	wp_enqueue_style( 'tcu_login_css', get_template_directory_uri() . '/library/css/wp-login.min.css', false );
}

/**
 * Changing the logo link from wordpress.org to your TCU website
 */
function tcu_web_standards_login_url() {
	return 'http://www.tcu.edu';
}

/**
 *  Changing the alt text on the logo to 'Texas Christian University'
 */
function tcu_web_standards_login_title() {
	return 'Texas Christian University';
}

// Calling it only on the login page.
add_action( 'login_enqueue_scripts', 'tcu_web_standards_login_css', 10 );
add_filter( 'login_headerurl', 'tcu_web_standards_login_url' );
add_filter( 'login_headertitle', 'tcu_web_standards_login_title' );

/**
 * Custom Backend Footer
 */
function tcu_web_standards_custom_admin_footer() {
	_e( '<span id="footer-thankyou">Managed by <a href="http://mkc.tcu.edu/web-management.asp" target="_blank">The Office of Website and Social Media Management</a> at TCU</span>', 'tcu_web_standards' );
}

// Adding it to the admin area.
add_filter( 'admin_footer_text', 'tcu_web_standards_custom_admin_footer' );
