<?php
/**
 * Supports the TCU framework guidelines. It customizes the WordPress Admin section.
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 4.8.0
 */

require_once 'tcu-style.php';
require_once 'tcu-google-analytics-settings.php';
require_once 'tcu-breadcrumbs-settings.php';

/**
 * Open 'editor' roles to use slider, widgets, and custom sidebars
 * Set the image link to none as default when using media
 */
function tcu_web_standards_theme_activate() {

	// Get Editor role and add the ability to edit menus, widgets, slideshow, sidebars.
	global $editor_roles;
	$editor_roles = get_role( 'editor' );

	// Check if the role exists.
	if ( ! empty( $editor_roles ) ) {

		// Add MENU AND WIDGET editing back to the Editor role.
		$editor_roles->add_cap( 'edit_theme_options' );

		// Checks if easingsliderpro plugin is installed.
		if ( function_exists( 'easingsliderpro' ) ) {

			// Add capabilities for the Easing Slider Pro plugin.
			$editor_roles->add_cap( 'easingsliderpro_edit_slideshow' );
			$editor_roles->add_cap( 'easingsliderpro_edit_slideshows' );
			$editor_roles->add_cap( 'easingsliderpro_can_customize' );
			$editor_roles->add_cap( 'easingsliderpro_edit_settings' );
			$editor_roles->add_cap( 'easingsliderpro_import_export_slideshows' );
			$editor_roles->add_cap( 'easingsliderpro_add_slideshow' );
		}

		// Compatibility for new easing slider plugin.
		if ( function_exists( 'easingslider' ) ) {

			// Add capabilities for the Easing Slider  plugin.
			$editor_roles->add_cap( 'easingslider_manage_settings' );
			$editor_roles->add_cap( 'easingslider_edit_sliders' );
			$editor_roles->add_cap( 'easingslider_manage_customizations' );
			$editor_roles->add_cap( 'easingslider_publish_sliders' );
		}
	}

	// Set link image to none by default.
	update_option( 'image_default_link_type', 'none' );
}

add_action( 'after_switch_theme', 'tcu_web_standards_theme_activate' );

/**
 * Remove capabilities from editor role
 * Delete theme options from database
 */
function tcu_theme_deactivate() {

	// Get Editor role and remove the ability to edit menus, widgets, slideshow, sidebars.
	global $editor_roles;
	$editor_roles = get_role( 'editor' );

	// Check if the role exists.
	if ( ! empty( $editor_roles ) ) {

		// Remove MENU AND WIDGET editing permissions to editor role.
		$editor_roles->remove_cap( 'edit_theme_options' );

		// Checks if easingsliderpro plugin is installed.
		// Compatibility for older version of easing slider plugin.
		if ( function_exists( 'easingsliderpro' ) ) {

			// Remove capabilities for the Easing Slider Pro plugin.
			$editor_roles->remove_cap( 'easingsliderpro_edit_slideshow' );
			$editor_roles->remove_cap( 'easingsliderpro_edit_slideshows' );
			$editor_roles->remove_cap( 'easingsliderpro_can_customize' );
			$editor_roles->remove_cap( 'easingsliderpro_edit_settings' );
			$editor_roles->remove_cap( 'easingsliderpro_import_export_slideshows' );
			$editor_roles->remove_cap( 'easingsliderpro_add_slideshow' );
		}

		// Compatibility for new easing slider plugin.
		if ( function_exists( 'easingslider' ) ) {

			// Add capabilities for the Easing Slider  plugin.
			$editor_roles->remove_cap( 'easingslider_manage_settings' );
			$editor_roles->remove_cap( 'easingslider_edit_sliders' );
			$editor_roles->remove_cap( 'easingslider_manage_customizations' );
			$editor_roles->remove_cap( 'easingslider_publish_sliders' );
		}
	}

	// Set image link back to default state.
	update_option( 'image_default_link_type', 'file' );
}

add_action( 'switch_theme', 'tcu_theme_deactivate' );
