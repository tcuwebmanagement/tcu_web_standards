<?php
/**
 * TCU Web Standards core functions and definitions
 *
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags.
 *
 * When using a child theme you can override certain functions (those wrapped in a
 * function_exists() call) by defining theme first in your child theme's function.php file
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in a function_exists() call) are
 * instead attached to a filter or action hook.
 *
 * @package tcu_web_standards
 * @since TCU Web Standards 1.0.0
 */

// we're firing all out initial functions at the start.
add_action( 'after_setup_theme', 'tcu_get_started', 16 );

/**
 * Firing all initial functions through
 * the after_setup_theme action hook.
 */
function tcu_get_started() {

	// launching operation cleanup.
	add_action( 'init', 'tcu_head_cleanup' );

	// remove WP version from RSS.
	add_filter( 'the_generator', 'tcu_rss_version' );

	// remove pesky injected css for recent comments widget.
	add_filter( 'wp_head', 'tcu_remove_wp_widget_recent_comments_style', 1 );

	// clean up comment styles in the head.
	add_action( 'wp_head', 'tcu_remove_recent_comments_style', 1 );

	// enqueue base scripts and styles.
	add_action( 'wp_enqueue_scripts', 'tcu_scripts_and_styles', 999 );

	// This theme styles the visual editor to resemble the theme style.
	add_editor_style( 'library/css/editor-styles.min.css' );

	// launching this stuff after theme setup.
	tcu_theme_support();

	// adding sidebars to WordPress (these are created in functions.php).
	add_action( 'widgets_init', 'tcu_register_sidebars' );
	add_action( 'widgets_init', 'tcu_register_footer_widgets' );

	// adding the tcu search form (created in functions.php).
	add_filter( 'get_search_form', 'tcu_wpsearch' );

	// cleaning up random code around images.
	add_filter( 'the_content', 'tcu_filter_ptags_on_images' );

	// cleaning up excerpt.
	add_filter( 'excerpt_more', 'tcu_excerpt_more' );
} /* end tcu_get_started */

/**
 * Check if the TCU_Admin plugin is intalled
 *
 * @return bool  True if the TCU_Admin plugin is installed
 */
function tcu_web_standards_admin_plugin() {

	/* Checks to see if "is_plugin_active" function exists and if not load the php file that includes that function */
	if ( ! function_exists( 'is_plugin_active' ) ) {
		include_once ABSPATH . 'wp-admin/includes/plugin.php';
	}

	/* Checks to see if the TCU Admin is activated  */
	if ( is_plugin_active( 'tcu_admin/tcu_customize.php' ) ) {
		return true;
	} else {
		return false;
	}
}


/**
 * Let's display a warning if the TCU_Admin plugin is installed.
 */
function tcu_web_standards_notice_remove_admin_plugin() {
	global $tcu_web_standards_notice_msg;

	$allowed_tags = array(
		'div' => array(
			'class' => array(),
		),
	);

	$tcu_web_standards_notice_msg = esc_html__( 'Warning: Please deactivate the TCU Admin plugin. The TCU Web Standards theme will not work properly when this plugin is active.', 'tcu_web_standards' );

	if ( tcu_web_standards_admin_plugin() ) {
		echo wp_kses( '<div class="notice notice-error notice-large"><div class="notice-title">' . $tcu_web_standards_notice_msg . '</div></div>', $allowed_tags );
	}
}

add_action( 'admin_notices', 'tcu_web_standards_notice_remove_admin_plugin' );

/**
 * WP_HEAD GOODNESS
 * The default WordPress head is
 * a mess. Let's clean it up by
 * removing all the junk we don't
 * need.
 */
function tcu_head_cleanup() {
	// EditURI link.
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer.
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// index link.
	remove_action( 'wp_head', 'index_rel_link' );
	// previous link.
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link.
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts.
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version.
	remove_action( 'wp_head', 'wp_generator' );
} /* end tcu head cleanup */

/**
 *  Remove WP version from RSS.
 */
function tcu_rss_version() {
	return '';
}

/**
 *  Remove injected CSS for recent comments widget.
 */
function tcu_remove_wp_widget_recent_comments_style() {
	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	}
}

/**
 *  Remove injected CSS from recent comments widget.
 */
function tcu_remove_recent_comments_style() {
	global $wp_widget_factory;
	if ( isset( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'] ) ) {
		remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
	}
}

/**
 *  A better title
 *
 * @link http://www.deluxeblogtips.com/2012/03/better-title-meta-tag.html
 *
 * @param string $title        The title of the page.
 * @param string $sep          How to separate the various items within the page title.
 * @param string $seplocation  Direction to display title.
 *
 * @return string $title        Returns the modified title.
 */
function tcu_rw_title( $title, $sep, $seplocation ) {
	global $page, $paged;

	// Don't affect in feeds.
	if ( is_feed() ) {
		return $title;
	}

	// Add the blog's name.
	if ( 'right' === $seplocation ) {
		$title .= get_bloginfo( 'name' );
	} else {
		$title = get_bloginfo( 'name' ) . $title;
	}

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );

	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title .= " {$sep} {$site_description}";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		/* translators: %s: Page number */
		$title .= " {$sep} " . sprintf( __( 'Page %s', 'tcu_web_standards' ), max( $paged, $page ) );
	}

	return $title;
} // End better title.

add_filter( 'wp_title', 'tcu_rw_title', 10, 3 );

/**
 * SCRIPTS & ENQUEUEING
 * Loading modernizr and jquery, and reply script
 */
function tcu_scripts_and_styles() {
	$theme_data    = wp_get_theme('tcu_web_standards');
	$theme_version = $theme_data->Version;

	// modernizr (without media query polyfill).
	wp_register_script( 'tcu-modernizr', get_template_directory_uri() . '/library/js/libs/modernizr-custom.js', array(), $theme_version, false );

	// Load the classList polyfill.
	wp_enqueue_script( 'classlist-polyfill', 'https://cdnjs.cloudflare.com/ajax/libs/classlist/1.2.20171210/classList.min.js', array(), $theme_version );
	wp_script_add_data( 'classlist-polyfill', 'conditional', 'lte IE 9' );

	// register main stylesheet.
	wp_register_style( 'tcu-stylesheet', get_template_directory_uri() . '/library/css/style.min.css', array(), $theme_version, 'all' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_register_style( 'tcu-ie-only', get_template_directory_uri() . '/library/css/ie.min.css', array( 'tcu-stylesheet' ), $theme_version );
	wp_enqueue_style( 'tcu-ie-only' );
	wp_style_add_data( 'tcu-ie-only', 'conditional', 'lt IE 9' );

	// comment reply script for threaded comments.
	if ( is_singular() && comments_open() && ( get_option( 'thread_comments' ) === 1 ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Register velocity.js.
	wp_register_script( 'tcu-velocity', get_template_directory_uri() . '/library/js/libs/velocity.min.js', array(), $theme_version, true );

	// Register mobile menu script.
	wp_register_script( 'tcu-meanmenu', get_template_directory_uri() . '/library/js/min/jquery.meanmenu.min.js', array( 'jquery' ), $theme_version, true );

	// Register menu scripts (dropdowns and side navigation).
	wp_register_script( 'tcu-menu', get_template_directory_uri() . '/library/js/min/tcu.menu.min.js', array( 'tcu-velocity' ), $theme_version, true );

	// Register mobile menu script.
	wp_register_script( 'tcu-responsivetabs', get_template_directory_uri() . '/library/js/libs/jquery.responsiveTabs.min.js', array( 'jquery' ), $theme_version, true );

	// Register accordions script.
	wp_register_script( 'tcu-accordions', get_template_directory_uri() . '/library/js/min/tcu.accordions.min.js', array( 'jquery' ), $theme_version, true );

	// Register scripts were we instatiate.
	wp_register_script( 'tcu-scripts', get_template_directory_uri() . '/library/js/min/scripts.min.js', array( 'jquery', 'tcu-velocity', 'tcu-meanmenu', 'tcu-menu', 'tcu-responsivetabs', 'tcu-accordions' ), $theme_version, true );

	// Enqueue styles and scripts.
	wp_enqueue_script( 'tcu-modernizr' );
	wp_enqueue_style( 'tcu-stylesheet' );

	// Enqueue our scripts.
	wp_enqueue_script( 'tcu-velocity' );
	wp_enqueue_script( 'tcu-meanmenu' );
	wp_enqueue_script( 'tcu-menu' );
	wp_enqueue_script( 'tcu-responsivetabs' );
	wp_enqueue_script( 'tcu-accordions' );
	wp_enqueue_script( 'tcu-scripts' );
}

/**
 * THEME SUPPORT
 * Adding WP 3+ Functions & Theme Support
 */
function tcu_theme_support() {

	// WP thumbnails (sizes handled in functions.php).
	add_theme_support( 'post-thumbnails' );

	// Default thumb size.
	set_post_thumbnail_size( 125, 125, true );

	// RSS feeds.
	add_theme_support( 'automatic-feed-links' );

	// WP menus.
	add_theme_support( 'menus' );

	// Registering wp3+ menus.
	register_nav_menus(
		array(
			'main-nav' => __( 'The Main Menu', 'tcu_web_standards' ),
		)
	);
} /* End tcu theme support */

/**
 * Numeric Page Navi (built into the theme by default)
 */
function tcu_page_navi() {
	global $wp_query;
	$bignum     = 999999999;
	$translated = __( 'Page Number ', 'tcu_web_standards' ); // Supply translatable string.

	if ( $wp_query->max_num_pages <= 1 ) {
		return;
	}

	$pages = '<nav aria-label="pagination" class="tcu-pagination">';

	$pages .= paginate_links(
		array(
			'base'               => str_replace( $bignum, '%#%', esc_url( get_pagenum_link( $bignum ) ) ),
			'format'             => '',
			'show_all'           => false,
			'current'            => max( 1, get_query_var( 'paged' ) ),
			'total'              => $wp_query->max_num_pages,
			'prev_text'          => '&larr; Previous Page',
			'next_text'          => 'Next &rarr;',
			'type'               => 'list',
			'end_size'           => 2,
			'mid_size'           => 2,
			'before_page_number' => '<span class = "tcu-visuallyhidden screen-reader-text">' . $translated . ' </span>',
		)
	);

	$pages .= '</nav>';

	$allowed_tags = array(
		'nav'  => array(
			'aria-label' => array(),
			'class'      => array(),
		),
		'ul'   => array(
			'class' => array(),
		),
		'li'   => array(
			'class' => array(),
		),
		'span' => array(
			'class' => array(),
		),
		'a'    => array(
			'class' => array(),
			'href'  => array(),
		),
	);

	echo wp_kses( $pages, $allowed_tags );
} /* end page navi */


if ( ! function_exists( 'tcu_breadcrumbs_list' ) ) {

	/**
	 * BREADCRUMBS
	 */
	function tcu_breadcrumbs_list() {

		/* === OPTIONS === */
		$text['cd_name'] = get_option( 'tcu_breadcrumbs_name' );
		$text['cd_url']  = get_option( 'tcu_breadcrumbs_url' );
		$text['home']    = get_bloginfo( 'name' );

		/* translators: %s: Taxonomy term name */
		$text['category'] = __( 'Archive for %s', 'tcu_web_standards' );

		/* translators: %s: Search term */
		$text['search'] = __( 'Search results for: %s', 'tcu_web_standards' );

		/* translators: %s: Tag term */
		$text['tag'] = __( 'Posts tagged %s', 'tcu_web_standards' );

		/* translators: %s: Author name */
		$text['author'] = __( 'View all posts by %s', 'tcu_web_standards' );
		$text['404']    = __( 'Error 404', 'tcu_web_standards' );

		$show['current'] = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
		$show['home']    = 1; // 1 - show breadcrumbs on the homepage, 0 - don't show

		$delimiter = '<span class="tcu-breadcrumbs__chevron">&#8250;</span> '; // Delimiter between crumbs.
		$before    = '<span class="tcu-breadcrumbs__current">'; // Tag before the current crumb.
		$after     = '</span>'; // tag after the current crumb
		/* === END OF OPTIONS === */

		/* === THE TCU.EDU LINK === */
		$tcu_link    = '<a class="tcu-icon-home" href="http://www.tcu.edu" title="Texas Christian University"><svg height="18" width="20" focusable="false"><use xlink:href="#home-icon"></use></svg><span class="tcu-visuallyhidden">Texas Christian University</span></a>';
		$cd_link     = '<span class="tcu-breadcrumbs__crumb" typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="' . $text['cd_url'] . '">' . $text['cd_name'] . '</a></span>';
		$home_link   = home_url( '/' );
		$before_link = '<span class="tcu-breadcrumbs__crumb" typeof="v:Breadcrumb">';
		$after_link  = '</span>';
		$link_att    = ' rel="v:url" property="v:title"';
		$link        = $before_link . '<a' . $link_att . ' href="%1$s">%2$s</a>' . $after_link;

		$post      = get_queried_object();
		$parent_id = isset( $post->post_parent ) ? $post->post_parent : '';

		$html_output = '';

		// Allowed HTML tags.
		$allowed_tags = array(
			'div'  => array(
				'class' => array(),
				'id'    => array(),
				'style' => array(),
			),
			'p'    => array(
				'class' => array(),
			),
			'nav'  => array(
				'aria-label' => array(),
				'class'      => array(),
				'xmlns:v '   => array(),
			),
			'span' => array(
				'aria-hidden' => array(),
				'class'       => array(),
			),
			'a'    => array(
				'aria-label' => array(),
				'title'      => array(),
				'class'      => array(),
				'href'       => array(),
			),
			'svg'  => array(
				'xmls'   => array(),
				'height' => array(),
				'width'  => array(),
				'style'  => array(),
			),
			'use'  => array(
				'xlink:href' => array(),
			),
		);

		if ( is_front_page() ) {
			// Show current post/page title in breadcrumbs.
			if ( 1 === $show['home'] ) {
				if ( ! empty( $text['cd_name'] ) && ! empty( $text['cd_url'] ) ) {
					$html_output .= '<nav aria-label="complementary" class="tcu-breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#"><div class="tcu-layout-constrain cf">' . $before_link . $tcu_link . $after_link . $delimiter . $cd_link . $delimiter . '<a aria-label="Go back to the homepage" href="' . $home_link . '">' . $text['home'] . '</a></div></nav>';
				} else {
					$html_output .= '<nav aria-label="complementary" class="tcu-breadcrumbs"><div class="tcu-layout-constrain cf">' . $before_link . $tcu_link . $after_link . $delimiter . '<a aria-label="Go back to the homepage" href="' . $home_link . '">' . $text['home'] . '</a></div></nav>';
				}
			}
			// If we are showing a unit/department.
		} else {
			// If the custom Breadcrumbs settings are empty.
			if ( ! empty( $text['cd_name'] ) && ! empty( $text['cd_url'] ) ) {
				$html_output .= '<nav aria-label="complementary" class="tcu-breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#"><div class="tcu-layout-constrain cf">' . $before_link . $tcu_link . $after_link . $delimiter . $cd_link . $delimiter . sprintf( $link, $home_link, $text['home'] ) . $delimiter;
			} else {
				$html_output .= '<nav aria-label="complementary" class="tcu-breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#"><div class="tcu-layout-constrain cf">' . $before_link . $tcu_link . $after_link . $delimiter . sprintf( $link, $home_link, $text['home'] ) . $delimiter;
			}

			if ( is_home() ) {
				if ( 1 === $show['current'] ) {
					$html_output .= $before . get_the_title( get_option( 'page_for_posts', true ) ) . $after;
				}
			} elseif ( is_category() ) {
				$this_cat = get_category( get_query_var( 'cat' ), false );
				if ( 0 !== $this_cat->parent ) {
					$cats = get_category_parents( $this_cat->parent, true, $delimiter );
					$cats = str_replace( '<a', $before_link . '<a' . $link_att, $cats );
					$cats = str_replace( '</a>', '</a>' . $after_link, $cats );

					$html_output .= $cats;
				}
				$html_output .= $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after;
			} elseif ( is_tax() ) {
				$post_type    = get_post_type_object( get_post_type() );
				$archive_link = get_post_type_archive_link( $post_type->name );
				$terms        = get_terms();
				$this_tax     = get_query_var( 'term' );
				$current_tax  = array_filter(
					$terms, function ( $x ) {
						return get_query_var( 'term' ) === $x->slug;
					}
				);
				$key          = key( $current_tax );
				$html_output .= sprintf( $link, $archive_link, $post_type->labels->singular_name );
				if ( 0 === $current_tax[ $key ]->parent ) {
					$post_type_link = sprintf( $link, $archive_link, $post_type->labels->singular_name );

					$html_output .= $delimiter . $before . $current_tax[ $key ]->name . $after;
				} else {
					$parent      = get_term_by( 'term_id', (int) $current_tax[ $key ]->parent, $current_tax[ $key ]->taxonomy );
					$parent_url  = get_term_link( (int) $parent );
					$parent_link = $delimiter . sprintf( $link, $parent_url, $parent->name );

					$html_output .= $parent_link . $delimiter . $current_tax[ $key ]->name;
				}
			} elseif ( is_search() ) {
				$html_output .= $before . sprintf( $text['search'], get_search_query() ) . $after;
			} elseif ( is_day() ) {
				$html_output .= sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
				$html_output .= sprintf( $link, get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ), get_the_time( 'F' ) ) . $delimiter;
				$html_output .= $before . get_the_time( 'd' ) . $after;
			} elseif ( is_month() ) {
				$html_output .= sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
				$html_output .= $before . get_the_time( 'F' ) . $after;
			} elseif ( is_year() ) {
				$html_output .= $before . get_the_time( 'Y' ) . $after;
			} elseif ( is_single() && ! is_attachment() ) {
				if ( 'post' !== get_post_type() ) {
					$post_type    = get_post_type_object( get_post_type() );
					$archive_link = get_post_type_archive_link( $post_type->name );
					$html_output .= sprintf( $link, $archive_link, $post_type->labels->singular_name );
					if ( 1 === $show['current'] ) {
						$html_output .= $delimiter . $before . get_the_title() . $after;
					}
				} else {
					$cat  = get_the_category();
					$cat  = $cat[0];
					$cats = get_category_parents( $cat, true, $delimiter );
					if ( 0 === $show['current'] ) {
						$cats = preg_replace( "#^(.+)$delimiter$#", '$1', $cats );
					}
					$cats = str_replace( '<a', $before_link . '<a' . $link_att, $cats );
					$cats = str_replace( '</a>', '</a>' . $after_link, $cats );

					$html_output .= $cats;

					if ( 1 === $show['current'] ) {
						$html_output .= $before . get_the_title() . $after;
					}
				}
			} elseif ( ! is_single() && ! is_page() && ! is_404() && 'post' !== get_post_type() ) {
				$post_type = get_post_type_object( get_post_type() );
				if ( $post_type ) {
					$html_output .= $before . $post_type->labels->singular_name . $after;
				}
			} elseif ( is_attachment() ) {
				$parent = get_post( $parent_id );
				$cat    = get_the_category( $parent->ID );

				if ( isset( $cat[0] ) ) {
					$cat = $cat[0];
				}

				if ( $cat ) {
					$cats = get_category_parents( $cat, true, $delimiter );
					$cats = str_replace( '<a', $before_link . '<a' . $link_att, $cats );
					$cats = str_replace( '</a>', '</a>' . $after_link, $cats );

					$html_output .= $cats;
				}

				$html_output .= sprintf( $link, get_permalink( $parent ), $parent->post_title );
				if ( 1 === $show['current'] ) {
					$html_output .= $delimiter . $before . get_the_title() . $after;
				}
			} elseif ( is_page() && ! $parent_id ) {
				if ( 1 === $show['current'] ) {
					$html_output .= $before . get_the_title() . $after;
				}
			} elseif ( is_page() && $parent_id ) {
				$breadcrumbs = array();
				while ( $parent_id ) {
					$page_child    = get_post( $parent_id );
					$breadcrumbs[] = sprintf( $link, get_permalink( $page_child->ID ), get_the_title( $page_child->ID ) );
					$parent_id     = $page_child->post_parent;
				}
				$breadcrumbs = array_reverse( $breadcrumbs );
				$crumb_count = count( $breadcrumbs );
				for ( $i = 0; $i < $crumb_count; $i++ ) {
					$html_output .= $breadcrumbs[ $i ];
					if ( $crumb_count - 1 !== $i ) {
						$html_output .= $delimiter;
					}
				}
				if ( 1 === $show['current'] ) {
					$html_output .= $delimiter . $before . get_the_title() . $after;
				}
			} elseif ( is_tag() ) {
				$html_output .= $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after;
			} elseif ( is_author() ) {
				$user_id  = get_query_var( 'author' );
				$userdata = get_the_author_meta( 'display_name', $user_id );

				$html_output .= $before . sprintf( $text['author'], $userdata ) . $after;

			} elseif ( is_404() ) {
				$html_output .= $before . $text['404'] . $after;
			}

			if ( get_query_var( 'paged' ) || get_query_var( 'page' ) ) {
				$page_num = get_query_var( 'page' ) ? get_query_var( 'page' ) : get_query_var( 'paged' );
				/* translators: %s: Page number */
				$html_output .= $delimiter . sprintf( __( 'Page %s', 'tcu_web_standards' ), $page_num );
			}

			$html_output .= '</div></nav>';
		}

		echo wp_kses( $html_output, $allowed_tags );

	} // End responsive_breadcrumb_lists.
} // don't remove this bracket!


/**
 * RANDOM CLEANUP ITEMS
 * remove the p from around imgs
 *
 * @link http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/
 *
 * @param string $content  The content to filter.
 */
function tcu_filter_ptags_on_images( $content ) {
	return preg_replace( '/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content );
}

/**
 * RANDOM CLEANUP ITEMS
 * This removes the […] to a Read More link
 *
 * @param string $more The 'more' string.
 */
function tcu_excerpt_more( $more ) {
	global $post;

	return '...  <a aria-label="' . __( 'Read more about ', 'tcu_web_standards' ) . get_the_title( $post->ID ) . '" class="excerpt-read-more" href="' . get_permalink( $post->ID ) . '" title="' . __( 'Read ', 'tcu_web_standards' ) . get_the_title( $post->ID ) . '">' . __( 'Read more &raquo;', 'tcu_web_standards' ) . '</a>';
}

add_filter( 'pre_set_site_transient_update_themes', 'tcu_web_standards_check_updates' );

/**
 * Connects to our theme repository
 * Updates our theme
 *
 * TEMP: Enable update check on every request. Normally you don't need this! This is for testing only!
 * set_site_transient('update_themes', null);
 *
 * @param string $transient The transient name.
 *
 * @return $transient Filters the value of a specific site transient before it is set.
 */
function tcu_web_standards_check_updates( $transient ) {
	global $wp_version;

	// Double check that the theme is active and valid.
	if ( empty( $transient->checked['tcu_web_standards'] ) ) {
		return $transient;
	}

	$api_url = 'https://webmanage.tcu.edu/api/';

	if ( function_exists( 'wp_get_theme' ) ) {

		$theme_data    = wp_get_theme( get_option( 'template' ) );
		$theme_version = $theme_data->Version;

	}

	$theme_base = get_option( 'template' );

	$request = array(
		'slug'    => $theme_base,
		'version' => $transient->checked[ $theme_base ],
	);

	// Start checking for an update.
	$send_for_check = array(
		'body'       => array(
			'action'  => 'theme_update',
			'request' => serialize( $request ),
			'api-key' => md5( get_bloginfo( 'url' ) ),
		),
		'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' ),
	);

	$raw_response = wp_remote_post( $api_url, $send_for_check );

	if ( ! is_wp_error( $raw_response ) && ( 200 === $raw_response['response']['code'] ) ) {
		$response = unserialize( $raw_response['body'] );
	}

	// Feed the update data into WP updater.
	if ( ! empty( $response ) ) {
		$transient->response[ $theme_base ] = $response;
	}

	return $transient;
}
